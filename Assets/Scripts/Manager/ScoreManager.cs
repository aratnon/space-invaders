﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager
{
    private int _currentScore;

    public int CurrentScore => _currentScore;

    public void Reset()
    {
        _currentScore = 0;
    }

    public void IncreaseScore(int score)
    {
        _currentScore += score;
    }
}
