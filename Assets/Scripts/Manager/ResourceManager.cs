﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu (menuName = "Resource Manager")]
public class ResourceManager : ScriptableObject
{
    [SerializeField] private BackgroundResources _backgroundResources;
    public BackgroundResources BackgroundResources => _backgroundResources;
}
