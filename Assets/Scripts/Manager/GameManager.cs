﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameManager
{
    public enum GameState
    {
        Play, Pause, GameOver
    }

    [Inject] private LevelDataFactory _levelDataFactory;
    [Inject] private EventManager _eventManager;
    
    public GameState CurrentGameState { get; private set; }
    public GameModel CurrentGameModel { get; private set; }
    public PlayerModel PlayerModel { get; private set; }
    public int CurrentWaveNumber
    {
        get => CurrentGameModel.WaveNumber;
        private set => CurrentGameModel.WaveNumber = value;
    }
    
    public int GameScore => (int) CurrentGameModel.GameScore;

    public int Gold => CurrentGameModel.GoldEarned;
    
    public GameManager()
    {
        Init();
    }
    
    private void Init()
    {
        CurrentGameState = GameState.Pause;
        CurrentGameModel = GameModel.Create();
    }

    public void Pause()
    {
        CurrentGameState = GameState.Pause;
    }

    public void Play(PlayerModel playerModel)
    {
        PlayerModel = playerModel;
        CurrentGameModel = GameModel.Create();
        CurrentGameState = GameState.Play;
        NextWave();
    }

    public void GameOver()
    {
        CurrentGameState = GameState.GameOver;
        _eventManager.TriggerEvent(EventConstant.GAME_OVER);
    }

    public void Restart()
    {
        
    }

    public void NextWave()
    {
        CurrentWaveNumber += 1;
        
        EventParam param = EventParam.Create();
        param.AddParam(EnemyWaveModel.WAVE_NUMBER, CurrentWaveNumber);
        
        _eventManager.TriggerEvent(EventConstant.NEXT_WAVE, param);
    }

    public void UpdatePlayerFuelTick(float dt)
    {
        PlayerModel.UpdateFuelTick += dt;
    }

    public void ResetPlayerFuelTick()
    {
        PlayerModel.UpdateFuelTick = 0;
    }

    public void UpdatePowerUpTime(float dt)
    {
        CurrentGameModel.PowerUpTime += dt;
    }

    public void ResetPowerUpTime()
    {
        CurrentGameModel.PowerUpTime = 0;
    }

    public void UpdateGameScore(float dt)
    {
        CurrentGameModel.ScoreTickCount += dt;
        if (!(CurrentGameModel.ScoreTickCount >= GameModel.SCORE_INCREASE_TICK_RATE)) return;
        CurrentGameModel.ScoreTickCount = 0;
        AddScore(Mathf.RoundToInt(GameModel.SCORE_INCREASE_RATE));
    }

    public void AddScore(int score)
    {
        CurrentGameModel.GameScore += score;
            
        EventParam eventParam = EventParam.Create();
        eventParam.AddParam(EventParamConstant.GAME_SCORE, CurrentGameModel.GameScore);
        _eventManager.TriggerEvent(EventConstant.UPDATE_GAME_SCORE);
    }

    public void AddGold(int gold)
    {
        CurrentGameModel.GoldEarned += gold;
        
        EventParam eventParam = EventParam.Create();
        eventParam.AddParam(EventParamConstant.GOLD_AMOUT, CurrentGameModel.GameScore);
        _eventManager.TriggerEvent(EventConstant.UPDATE_GOLD);
    }

    public void IncreasePlayerGunLevel()
    {
        PlayerModel.IncreaseGunLevel();
    }
    
    public void AddPlayerHealth()
    {
        PlayerModel.ModifyCurrentHealth(PlayerModel.FuelRestorePoint);
    }

    public void ActivatePlayerShield()
    {
        PlayerModel.PlasmaShieldActive = true;
    }

    public void UpdatePlayerShieldTime(float dt)
    {
        PlayerModel.CurrentPlasmaShieldTime += dt;

        if (PlayerModel.CurrentPlasmaShieldTime < PlayerModel.MaxPlasmaShieldTime) return;

        PlayerModel.CurrentPlasmaShieldTime = 0;
        PlayerModel.PlasmaShieldActive = false;
        
        _eventManager.TriggerEvent(EventConstant.DEACTIVATE_PLASMA_SHIELD);
    }
}
