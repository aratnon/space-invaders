﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ObjectPoolManager : MonoBehaviour, IObjectPoolManager
{
    [Inject] private UnityUtility _unityUtility;
    
    [SerializeField] private List<ObjectPool> _prefabPools;

    private Dictionary<string, List<GameObject>> _objectPools;

    public ObjectPoolManager(UnityUtility unityUtility)
    {
        _unityUtility = unityUtility;
    }
    
    public void CreatePools(Action onSuccess)
    {
        _objectPools = new Dictionary<string, List<GameObject>>();
        _prefabPools.ForEach(prefabPool =>
        {
            List<GameObject> pools = new List<GameObject>();
            for (int i = 0; i != prefabPool.Count; ++i)
            {
                GameObject prefab = prefabPool.Prefab;
                GameObject clone = _unityUtility.CreateGameObject(prefab, transform);
                var poolableObject = clone.GetComponent<PoolableObject>();
                poolableObject.ObjectId = prefabPool.ObjectId;
                clone.gameObject.SetActive(false);
                pools.Add(clone);
            }
            _objectPools.Add(prefabPool.ObjectId, pools);
        });
        onSuccess.Invoke();
    }
    
    public GameObject GetPoolableObject(string id)
    {
        GameObject poolableObject = _objectPools[id].Find(clone => !clone.activeSelf);
        poolableObject.SetActive(true);
        return poolableObject;
    }
    
    public T GetComponentFromPoolableObject<T>(string id)
    {
        GameObject poolableObject = GetPoolableObject(id);
        return poolableObject.GetComponent<T>();
    }

    private IEnumerator StartSetActivePoolableObject(GameObject target, float delay, bool active)
    {
        yield return new WaitForSeconds(delay);
        target.SetActive(active);
    }
    
    public void SetActivePoolableObject(GameObject target, float delay, bool active)
    {
        StartCoroutine(StartSetActivePoolableObject(target, delay, active));
    }

    public void SetActiveAllPoolableObjectByKey(string id, bool active)
    {
        _objectPools[id].ForEach(obj => obj.SetActive(active));
    }

    public void DisablePoolableObjectOnGameRestart()
    {
        _prefabPools.ForEach(objPool =>
        {
            if (objPool.ClearOnRestartGame)
            {
                SetActiveAllPoolableObjectByKey(objPool.ObjectId, false);
            }
        });
    }
}
