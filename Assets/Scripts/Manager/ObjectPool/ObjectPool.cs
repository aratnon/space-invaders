﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [SerializeField] private bool clearOnRestartGame;
    [SerializeField] private int count;
    [SerializeField] private string objectId;
    [SerializeField] private GameObject pooledPrefab;

    public bool ClearOnRestartGame => clearOnRestartGame;
    public int Count => count;
    public string ObjectId => objectId;
    public GameObject Prefab => pooledPrefab;
    
}
