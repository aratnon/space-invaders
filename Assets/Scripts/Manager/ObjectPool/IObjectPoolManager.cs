﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IObjectPoolManager
{
    void CreatePools(Action onSuccess);
}
