using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ScriptableInstaller", menuName = "Installers/GameResourceInstaller")]
public class GameResourceInstaller : ScriptableObjectInstaller<GameResourceInstaller>
{
    [SerializeField] private GameConstant _gameConstant;
    [SerializeField] private ResourceManager _resourceManager;
    public override void InstallBindings()
    {
        Container.BindInstance(_gameConstant).AsSingle();
        Container.BindInstance(_resourceManager).AsSingle();
    }
}