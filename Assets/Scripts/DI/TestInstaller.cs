﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TestInstaller : Installer<TestInstaller>
{
    public override void InstallBindings()
    {
        InstallGame();
        InstallUi();
        InstallGunBindings();
        InstallPlayerBindings();
        InstallPowerUpBindings();
        InstallGoldDropBindings();
        InstallEnemyBindings();
        InstallBackgroundBindings();
    }

    private void InstallGame()
    {
//        Container.Bind<Camera>().WithId(InjectionIds.Id.MainCamera).FromInstance(mainCamera);
        Container.Bind<IGameUtility>().FromInstance(GameUtility.Create()).AsSingle();
        Container.Bind<UnityUtility>().AsSingle();
        Container.Bind<EventManager>().AsSingle();
        Container.Bind<GameManager>().FromMethod(CreateGameManager);
        Container.Bind<LevelDataFactory>().AsSingle();
        Container.Bind<GunModelFactory>().AsSingle();
        Container.Bind<LevelController>().AsSingle();
        Container.Bind<SaveGameManager>().FromMethod(CreateSaveGameManager).AsSingle();
    }

    private GameManager CreateGameManager()
    {
        var gameManager = new GameManager();
        Container.Inject(gameManager);
        
        gameManager.Play(PlayerModel.Create(10, 10));

        return gameManager;
    }

    private SaveGameManager CreateSaveGameManager()
    {
        var saveGameManager = new SaveGameManager();
        Container.Inject(saveGameManager);
        
        saveGameManager.Init(true);

        return saveGameManager;
    }

    private void InstallUi()
    {
        Container.Bind<LoadingController>().AsTransient();
        Container.Bind<MainMenuController>().AsTransient();
        Container.Bind<GameHudController>().AsTransient();
        Container.Bind<GameOverController>().AsTransient();
        Container.Bind<ShopController>().AsTransient();
    }
    
    private void InstallBackgroundBindings()
    {
        Container.Bind<RepeatingBackgroundController>().AsTransient();
        Container.Bind<BackgroundController>().AsTransient();
    }

    private void InstallGunBindings()
    {
        Container.Bind<BaseBulletSpanwerController>().AsTransient();
        Container.Bind<BulletController>().AsTransient();
        Container.Bind<BaseGunController>().AsTransient();
    }

    private void InstallPlayerBindings()
    {
        Container.Bind<PlayerMovementController>().AsTransient();
        Container.Bind<PlayerController>().AsTransient();
    }

    private void InstallPowerUpBindings()
    {
        Container.Bind<PowerUpController>().AsTransient();
    }

    private void InstallEnemyBindings()
    {
        Container.Bind<EnemyController>().AsTransient();
        Container.Bind<EnemyWaveController>().AsTransient();
    }
    
    private void InstallGoldDropBindings()
    {
        Container.Bind<GoldDropController>().AsTransient();
    }
}
