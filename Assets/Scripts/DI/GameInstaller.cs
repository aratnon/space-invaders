
using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller
{
    [SerializeField] private Camera mainCamera;
    [SerializeField] private ObjectPoolManager objectPoolManagerPrefab;
    [SerializeField] private PowerUpDataManager powerUpDataManagerPrefab;
    public override void InstallBindings()
    {
        InstallGame();
        InstallUi();
        InstallGunBindings();
        InstallPlayerBindings();
        InstallPowerUpBindings();
        InstallGoldDropBindings();
        InstallEnemyBindings();
        InstallBackgroundBindings();
    }

    private void InstallGame()
    {
        Container.Bind<Camera>().WithId(InjectionIds.Id.MainCamera).FromInstance(mainCamera);
        Container.Bind<IGameUtility>().FromInstance(GameUtility.Create()).AsSingle();
        Container.Bind<UnityUtility>().AsSingle();
        Container.Bind<EventManager>().AsSingle();
        Container.Bind<GameManager>().AsSingle();
        Container.Bind<SaveGameManager>().AsSingle();
        Container.Bind<LevelDataFactory>().AsSingle();
        Container.Bind<GunModelFactory>().AsSingle();
        Container.Bind<ObjectPoolManager>().FromMethod(CreateObjectPoolManager).AsSingle();
        Container.Bind<PowerUpDataManager>().FromMethod(CreatePowerUpDataManager).AsSingle();
        Container.Bind<LevelController>().AsSingle();
    }

    private PowerUpDataManager CreatePowerUpDataManager()
    {
        return Container.InstantiatePrefab(powerUpDataManagerPrefab).GetComponent<PowerUpDataManager>();
    }
    
    private ObjectPoolManager CreateObjectPoolManager()
    {
        return Container.InstantiatePrefab(objectPoolManagerPrefab).GetComponent<ObjectPoolManager>();
    }
    
    

    private void InstallUi()
    {
        Container.Bind<LoadingController>().AsTransient();
        Container.Bind<MainMenuController>().AsTransient();
        Container.Bind<GameHudController>().AsTransient();
        Container.Bind<GameOverController>().AsTransient();
        Container.Bind<ShopController>().AsTransient();
    }
    
    private void InstallBackgroundBindings()
    {
        Container.Bind<RepeatingBackgroundController>().AsTransient();
        Container.Bind<BackgroundController>().AsTransient();
    }

    private void InstallGunBindings()
    {
        Container.Bind<BaseBulletSpanwerController>().AsTransient();
        Container.Bind<BulletController>().AsTransient();
        Container.Bind<BaseGunController>().AsTransient();
    }

    private void InstallPlayerBindings()
    {
        Container.Bind<PlayerMovementController>().AsTransient();
        Container.Bind<PlayerController>().AsTransient();
    }

    private void InstallPowerUpBindings()
    {
        Container.Bind<PowerUpController>().AsTransient();
    }

    private void InstallEnemyBindings()
    {
        Container.Bind<EnemyController>().AsTransient();
        Container.Bind<EnemyWaveController>().AsTransient();
    }

    private void InstallGoldDropBindings()
    {
        Container.Bind<GoldDropController>().AsTransient();
    }
}