﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventParam
{
    private Dictionary<string, object> parameters = new Dictionary<string, object>();

    public static EventParam Create()
    {
        return new EventParam();
    }
    
    public void AddParam(string key, object value)
    {
        parameters[key] = value;
    }

    public T GetParam<T>(string key)
    {
        return (T)parameters[key];
    }
}
