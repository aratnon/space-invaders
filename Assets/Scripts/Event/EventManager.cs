﻿using System;
using UnityEngine.Events;
using System.Collections.Generic;
using UnityEngine;

public class EventManager {

    private Dictionary <string, Action<EventParam>> _eventDictionary = new Dictionary<string, Action<EventParam>>();
    
    public Action<EventParam> StartListening (string eventName, Action<EventParam> listener)
    {
        if (_eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent += listener;
            _eventDictionary[eventName] = thisEvent;
        } 
        else
        {
            thisEvent +=listener;
            _eventDictionary.Add (eventName, thisEvent);
        }

        return listener;
    }

    public void StopListening (string eventName, Action<EventParam> listener)
    {
        if (_eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent -= listener;
            _eventDictionary[eventName] = thisEvent;
        }
    }

    public void TriggerEvent (string eventName, EventParam eventParam)
    {
        if (_eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent?.Invoke (eventParam);
        }
    }
    
    public void TriggerEvent (string eventName)
    {
        if (_eventDictionary.TryGetValue (eventName, out var thisEvent))
        {
            thisEvent.Invoke (null);
        }
    }
}