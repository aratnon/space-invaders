﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class SaveGameManager
{
    private static string SAVED_GAME_PATH = "/save.s";
    public SaveGame SaveGame { get; private set; }
    private bool _isTest;

    public void Init(bool test)
    {
        _isTest = test;
        if (!test)
        {
            LoadSaveFromFile();
        }
        else
        {
            SaveGame = SaveGame.Create();
        }
    }
    
    public void LoadSaveFromFile()
    {
        SaveGame = SaveGame.Create();
        if (SaveFileExists())
        {
            var data = File.ReadAllText(GetSaveFilePath());
            SaveGame = JsonConvert.DeserializeObject<SaveGame>(data);
        }
        else
        {
            Save();
        }
    }

    private static string GetSaveFilePath()
    {
        return Application.persistentDataPath + SAVED_GAME_PATH;
    }

    private static bool SaveFileExists()
    {
        return File.Exists(GetSaveFilePath());
    }
    
    public void Save()
    {
        if (!_isTest)
        {
            var data = JsonConvert.SerializeObject(SaveGame, Formatting.Indented);
            File.WriteAllText(GetSaveFilePath(), data);
        }
    }
}
