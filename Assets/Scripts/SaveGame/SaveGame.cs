﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using UnityEngine;

public class SaveGame
{
    [JsonProperty] public int BestScore { get; set; }

    [JsonProperty] public int Gold { get; set; }

    [JsonProperty] private Dictionary<string, int> PowerUpLevels { get; set; }
    
    public static SaveGame Create()
    {
        return new SaveGame
        {
            BestScore = 0,
            Gold = 0,
            PowerUpLevels = CreatePowerUpLevels(),
        };
    }

    private static Dictionary<string, int> CreatePowerUpLevels()
    {
        return new Dictionary<string, int>
        {
            { PowerUpKey.FUEL_UPGRADE_KEY, 0 },
            { PowerUpKey.PLASMA_SHIELD_UPGRADE_KEY, 0 }
        };
    }

    public int GetPowerUpLevel(string key) => PowerUpLevels.ContainsKey(key) ? PowerUpLevels[key] : 0;
    public void UpgradePowerUp(string key, int maxLevel) => PowerUpLevels[key] = Mathf.Clamp(GetPowerUpLevel(key) + 1, 0, maxLevel);

    public void ModifyGold(int value) => Gold = (int) Mathf.Clamp(Gold + value, 0, Int64.MaxValue);

}
