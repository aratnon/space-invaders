﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventParamConstant
{
    public static readonly string UNIT_ID = "UNIT_ID";
    public const string BULLET_DAMAGE = "BULLET_DAMAGE";
    public const string POWER_UP_TYPE = "POWER_UP_TYPE";
    public const string GOLD_AMOUT = "GOLD_AMOUT";


    public const string GAME_SCORE = "GAME_SCORE";
    public static readonly string GAME_TIME = "GAME_TIME";

    public const string PLAYER_POSITION = "PLAYER_POSITION";
    public const string ENEMY_POSITION = "ENEMY_POSITION";
}
