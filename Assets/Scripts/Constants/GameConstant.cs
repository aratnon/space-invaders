﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[CreateAssetMenu (menuName = "Game Constant")]
public class GameConstant: ScriptableObject
{
    [SerializeField] private float _initialBackgroundSpeed;
    public float InitialBackgroundSpeed => _initialBackgroundSpeed;
}
