﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tag
{
    public const string PLAYER = "Player";
    public const string ENEMY = "Enemy";
    public const string PLAYER_BULLET = "PlayerBullet";
    public const string ENEMY_BULLET = "EnemyBullet";
    public const string SHIELD = "Shield";
    public const string BOUNDARY = "Boundary";
    public const string POWERUP = "PowerUp";
    public const string SUPPLY = "Supply";
}
