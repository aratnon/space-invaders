﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventConstant
{
    public static readonly string GAME_START = "GAME_START";
    public static readonly string CLICK_GAME_START = "CLICK_GAME_START";
    public static readonly string CLICK_RESTART = "CLICK_RESTART";
    public static readonly string GAME_OVER = "GAME_OVER";
    public static readonly string NEXT_WAVE = "NEXT_WAVE";
    public static readonly string FINISH_WAVE = "FINISH_WAVE";
    public static readonly string CREATE_WAVE = "CREATE_WAVE";
    public static readonly string FINISHED_ENEMY = "FINISHED_ENEMY";
    public static readonly string ADD_SCORE = "ADD_SCORE";
    public static readonly string ADD_GOLD = "ADD_GOLD";
    
    
    public static readonly string DEAL_DAMAGE_TO_PLAYER = "DEAL_DAMAGE_TO_PLAYER";
    public static readonly string DEAL_DAMAGE_TO_ENEMY = "DEAL_DAMAGE_TO_ENEMY";
    
    public static readonly string PLAYER_FIRE = "PLAYER_FIRE";
    public static readonly string RUN_OUT_OF_FUEL = "RUN_OUT_OF_FUEL";
    public static readonly string ENEMY_FIRE = "ENEMY_FIRE";
    public static readonly string ENEMY_DIE = "ENEMY_DIE";
    
    public static readonly string BULLET_HIT_PLAYER = "BULLET_HIT_PLAYER";
    public static readonly string POWER_UP_HIT_PLAYER = "POWER_UP_HIT_PLAYER";
    public static readonly string GOLD_HIT_PLAYER = "GOLD_HIT_PLAYER";
    public static readonly string BULLET_HIT_ENEMY = "BULLET_HIT";
    
    public static readonly string ACTIVATE_POWER_UP = "ACTIVATE_POWER_UP";
    public static readonly string DEACTIVATE_PLASMA_SHIELD = "DEACTIAVE_SHIELD";
    
    public static readonly string UPDATE_GAME_SCORE = "UPDATE_GAME_SCORE";
    public static readonly string UPDATE_GOLD = "UPDATE_GOLD";
    public static readonly string UPDATE_PLAYER_FUEL_LEVEL = "UPDATE_PLAYER_FUEL_LEVEL";
    public static readonly string UPDATE_PLAYER_FUEL_BAR_POSITION = "UPDATE_PLAYER_FUEL_BAR_POSITION";
    
    public static readonly string SHOW_SHOP_SCREEN = "SHOW_SHOP_SCREEN";
    
    public static string GetPlayerFireEventTag(int gunLevel)
    {
        return $"{PLAYER_FIRE}_{gunLevel}";
    }
    
    public static string GetEnemyFireEventTag(int enemyId)
    {
        return $"{ENEMY_FIRE}_{enemyId}";
    }

    public static string GetEnemyBulletHitEventTag(int enemyIndex)
    {
        return $"{BULLET_HIT_ENEMY}_{enemyIndex}";
    }
    
    public static string GetEnemyBulletHitEventTag(string enemyName)
    {
        return $"{BULLET_HIT_ENEMY}_{enemyName}";
    }
    
    public static string GetPlayerBulletHitEventTag()
    {
        return BULLET_HIT_PLAYER;
    }
}