﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class ParticleController : PoolableObject
{
    [Inject] private ObjectPoolManager _objectPoolManager;
    
    [SerializeField] private float activeTime;
    [SerializeField] private List<ParticleSystem> _subParticleSystems;

    public ParticleController PlayParticle()
    {
        _subParticleSystems.ForEach(particle =>
        {
            particle.Clear();
            particle.Play();
        });
        return this;
    }

    public void StopWhenFinishing()
    {
        _objectPoolManager.SetActivePoolableObject(gameObject, activeTime, false);
    }
}
