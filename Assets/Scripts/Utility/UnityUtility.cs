﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class UnityUtility
{
    [Inject] private DiContainer _diContainer;
    
    public GameObject CreateGameObject(MonoBehaviour prefab, Transform parent)
    {
        GameObject gameObject = _diContainer.InstantiatePrefab(prefab);
        gameObject.transform.SetParent(parent);
        return gameObject;
    }
    
    public GameObject CreateGameObject(GameObject prefab, Transform parent)
    {
        GameObject gameObject = _diContainer.InstantiatePrefab(prefab);
        gameObject.transform.SetParent(parent);
        return gameObject;
    }
    
    public T CreateGameObject<T>(GameObject prefab, Transform parent)
    {
        GameObject gameObject = _diContainer.InstantiatePrefab(prefab);
        gameObject.transform.SetParent(parent);
        return gameObject.GetComponent<T>();
    }

    public GameObject CreateRandomGameObjectFromList(List<GameObject> list, Transform parent)
    {
        GameObject prefab = GetRandomObjectFromList(list);
        return CreateGameObject(prefab, parent);
    }
    
    public T CreateRandomGameObjectFromList<T>(List<T> list, Transform parent) where T: MonoBehaviour
    {
        GameObject prefab = GetRandomObjectFromList(list).gameObject;
        return CreateGameObject<T>(prefab, parent);
    }

    public T GetRandomObjectFromList<T>(List<T> list)
    {
        return list[Random.Range(0, list.Count)];
    }
}
