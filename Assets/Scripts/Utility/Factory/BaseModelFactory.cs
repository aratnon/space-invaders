﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BaseModelFactory
{
    protected static float GetIncreasingValue(int waveNumber, float baseValue, float baseValueIncrease)
    {
        return baseValue + (waveNumber / 3.0f) * baseValue * baseValueIncrease;
    }
}
