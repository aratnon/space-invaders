﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBaseModelFactory: BaseModelFactory
{
    private static readonly float BASE_SPEED_INCREASE = 0.025f;
    
    public static BulletModel CreateBulletModel(
        int damage, 
        float speed,
        string source, 
        string bulletKey)
    {
        return new BulletModel
        {
            Damage = damage,
            Speed = speed,
            Source = source,
            BulletKey = bulletKey
        };
    }
    
    public static BulletModel CreateEnemyBulletModel(
        int waveNumber,
        int damage, 
        float speed,
        string source, 
        string bulletKey)
    {
        return new BulletModel
        {
            Damage = damage,
            Speed = GetEnemyBulletSpeed(waveNumber, speed),
            Source = source,
            BulletKey = bulletKey
        };
    }

    private static float GetEnemyBulletSpeed(int waveNumber, float baseSpeed)
    {
        return GetIncreasingValue(waveNumber, baseSpeed, BASE_SPEED_INCREASE);
    }
}
