﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelModelFactory : BaseModelFactory
{
    private static readonly float BASE_BACKGROUND_MOVING_SPEED_INCREASE = 0.015f;
    
    public static float GetGameSpeed(int waveNumber, float baseSpeed)
    {
        return -GetIncreasingValue(waveNumber, baseSpeed, BASE_BACKGROUND_MOVING_SPEED_INCREASE);
    }
}
