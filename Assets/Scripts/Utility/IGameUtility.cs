﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameUtility
{
    int RandomRange(int min, int max);
    float RandomRange(float min, float max);
}
