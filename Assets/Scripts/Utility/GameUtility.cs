﻿using UnityEngine;

public class GameUtility  : IGameUtility
{
    public static IGameUtility Create()
    {
        return new GameUtility();
    }
    
    public int RandomRange(int min, int max)
    {
        return Random.Range(min, max);
    }

    public float RandomRange(float min, float max)
    {
        return Random.Range(min, max);
    }
}
