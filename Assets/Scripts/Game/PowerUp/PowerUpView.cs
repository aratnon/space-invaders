﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PowerUpView : BaseView, IPowerUpView
{
    [Inject] private PowerUpController _powerUpController;
    [SerializeField] private Sprite gunUpgradeSprite, healthSprite, shieldSprite;
    [SerializeField] private AudioClip pickSound;
    private SpriteRenderer _spriteRenderer;
    
    private Action<EventParam> _activatePowerUpEvent;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        _spriteRenderer = GetComponent <SpriteRenderer>();
    }

    private void Start()
    {
        _powerUpController.View = this;
        _powerUpController.Start();
    }

    private void Update()
    {
        _powerUpController.UpdatePosition();
    }

    private void OnEnable()
    {
        _activatePowerUpEvent = _eventManager.StartListening(EventConstant.ACTIVATE_POWER_UP,
            param =>
            {
                PowerUpConstant.PowerUpType powerUpType =
                    param.GetParam<PowerUpConstant.PowerUpType>(EventParamConstant.POWER_UP_TYPE);
                _powerUpController.OnPowerUpActivated(powerUpType);
            });
    }

    private void OnDisable()
    {
        _eventManager.StopListening(EventConstant.ACTIVATE_POWER_UP, _activatePowerUpEvent);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        _powerUpController.OnTriggerEntered(collision.tag);
    }
    
    void OnTriggerExit2D(Collider2D collision)
    {
        _powerUpController.OnTriggerExited(collision.tag);
    }

    public void PlayPickSound()
    {
        PlaySound(pickSound, false);
    }

    public void StartMoving(float moveSpeed)
    {
        transform.Translate(Time.deltaTime * moveSpeed * Vector3.up); 
    }

    public void SetInitialPosition(float x, float y)
    {
        transform.position = new Vector3(x, y);
    }
    
    public void SetGunUpgradeSprite()
    {
        _spriteRenderer.sprite = gunUpgradeSprite;
    }

    public void SetHealthSprite()
    {
        _spriteRenderer.sprite = healthSprite;
    }

    public void SetShieldSprite()
    {
        _spriteRenderer.sprite = shieldSprite;
    }
}
