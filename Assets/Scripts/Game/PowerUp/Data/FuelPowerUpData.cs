﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Fuel Power Up Data")]
public class FuelPowerUpData : BasePowerUpData
{
    [SerializeField] private List<int> fuelPoints;

    public List<int> FuelPoints => fuelPoints;
}
