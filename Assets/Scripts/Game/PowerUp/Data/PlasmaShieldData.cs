﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Plasma Shield Data")]
public class PlasmaShieldData : BasePowerUpData
{
    [SerializeField] private List<int> shieldDurations;

    public List<int> ShieldDurations => shieldDurations;
}
