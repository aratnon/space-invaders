﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class PowerUpDataManager : MonoBehaviour
{
    [Inject] private SaveGameManager _saveGameManager;
    
    [SerializeField] private FuelPowerUpData fuelPowerUpData;
    [SerializeField] private PlasmaShieldData plasmaShieldData;

    private Dictionary<string, BasePowerUpData> _powerUpData;
    private SaveGame SaveGame => _saveGameManager.SaveGame;
    public FuelPowerUpData FuelPowerUpData => fuelPowerUpData;
    public List<int> FuelPowerUpPoints => fuelPowerUpData.FuelPoints;
    public int CurrentFuelPowerUpPoint => FuelPowerUpPoints[SaveGame.GetPowerUpLevel(PowerUpKey.FUEL_UPGRADE_KEY)];

    public PlasmaShieldData PlasmaShieldData => plasmaShieldData;
    public List<int> PlasmaShieldDurations => plasmaShieldData.ShieldDurations;
    public int CurrentPlasmaShieldDuration => PlasmaShieldDurations[SaveGame.GetPowerUpLevel(PowerUpKey.PLASMA_SHIELD_UPGRADE_KEY)];
    
    private void Awake()
    {
        _powerUpData = new Dictionary<string, BasePowerUpData>
        {
            [fuelPowerUpData.Key] = fuelPowerUpData, 
            [plasmaShieldData.Key] = plasmaShieldData
        };
    }

    public List<BasePowerUpModel> CreatePowerUpModels()
    {
        var powerUpModels = new List<BasePowerUpModel>
        {
            FuelPowerUpUpgradeModel.Create(FuelPowerUpData),
            PlasmaShieldPowerUpUpgradeModel.Create(PlasmaShieldData)
        };
        
        return powerUpModels;
    }

    public Sprite GetIcon(string key)
    {
        return _powerUpData[key].Icon;
    }
}
