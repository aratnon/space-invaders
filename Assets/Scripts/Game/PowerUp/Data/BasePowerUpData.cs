﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasePowerUpData : ScriptableObject
{
    [SerializeField] protected int maxLevel;
    [SerializeField] protected string key;
    [SerializeField] protected Sprite icon;
    [SerializeField] protected List<int> upgradeCosts;

    public int MaxLevel => maxLevel;
    public string Key => key;
    public Sprite Icon => icon;
    public List<int> UpgradeCosts => upgradeCosts;
}
