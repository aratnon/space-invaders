﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlasmaShieldPowerUpUpgradeModel : BasePowerUpModel
{
    private List<int> ShieldDurations { get; set; }


    public static PlasmaShieldPowerUpUpgradeModel Create(PlasmaShieldData plasmaShieldData)
    {
        return new PlasmaShieldPowerUpUpgradeModel
        {
            MaxLevel = plasmaShieldData.MaxLevel,
            Key = plasmaShieldData.Key,
            UpgradeCosts = plasmaShieldData.UpgradeCosts,
            ShieldDurations = plasmaShieldData.ShieldDurations
        };
    }
    
    private int CurrentShieldDuration => ShieldDurations[CurrentLevel];
    private int NextShieldDuration => ShieldDurations[NextLevel];

    public override string GetName()
    {
        return "Plasma Shield";
    }

    public override string GetDescription()
    {
        return $"Activate a plasma shield which can absorb all bullets for {CurrentShieldDuration} seconds.";
    }

    public override string GetNextLevelValue()
    {
        return $"Next Level: {NextShieldDuration} seconds";
    }

}
