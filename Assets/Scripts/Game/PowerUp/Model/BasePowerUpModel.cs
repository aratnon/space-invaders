﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;

public abstract class BasePowerUpModel
{
    public int CurrentLevel { get; set; }
    public int MaxLevel { get; set; }
    public string Key { get; set; }
    public List<int> UpgradeCosts { get; set; }
    public abstract string GetName();
    public abstract string GetDescription();
    
    public abstract string GetNextLevelValue();

    public int CurrentUpgradeCost => UpgradeCosts[CurrentLevel];

    public int NextLevel => Mathf.Clamp(CurrentLevel + 1, 0, MaxLevel);

    public bool IsMaxLevel => CurrentLevel == MaxLevel;

    public string LevelString => $"Level: {CurrentLevel + 1}/{MaxLevel + 1}";
    
}

public class PowerUpKey
{
    public const string FUEL_UPGRADE_KEY = "FUEL_UPGRADE";
    public const string PLASMA_SHIELD_UPGRADE_KEY = "PLASMA_SHIELD_UPGRADE";
}
