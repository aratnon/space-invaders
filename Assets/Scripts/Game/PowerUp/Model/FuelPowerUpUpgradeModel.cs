﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FuelPowerUpUpgradeModel : BasePowerUpModel
{
    public static FuelPowerUpUpgradeModel Create(FuelPowerUpData fuelPowerUpData)
    {
        return new FuelPowerUpUpgradeModel
        {
            CurrentLevel = 0,
            MaxLevel = fuelPowerUpData.MaxLevel,
            Key = fuelPowerUpData.Key,
            UpgradeCosts = fuelPowerUpData.UpgradeCosts,
            FuelPoints = fuelPowerUpData.FuelPoints
        };
    }
    
    private List<int> FuelPoints { get; set; }
    private int CurrentFuelPoint => FuelPoints[CurrentLevel];
    private int NextFuelPoint => FuelPoints[NextLevel];
    
    public override string GetName()
    {
        return "Fuel Pack";
    }

    public override string GetDescription()
    {
        return $"Restore Ship's fuel by: {CurrentFuelPoint} points";
    }

    public override string GetNextLevelValue()
    {
        return $"Next Level: {NextFuelPoint} points";
    }
}
