﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPowerUpView : IView
{
    void PlayPickSound();
    void StartMoving(float moveSpeed);
    void SetInitialPosition(float x, float y);
    void SetGunUpgradeSprite();
    void SetHealthSprite();
    void SetShieldSprite();
}
