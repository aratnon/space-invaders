﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpConstant
{
    [Serializable]
    public enum PowerUpType
    {
        GunUpgrade, Health, Shield, Gold
    }

    public static readonly List<PowerUpType> _powerUpTypes = new List<PowerUpType>
    {
        PowerUpType.GunUpgrade,
        PowerUpType.Health,
        PowerUpType.Shield
    };
}
