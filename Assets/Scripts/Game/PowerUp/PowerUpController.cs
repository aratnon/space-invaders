﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : BaseController<IPowerUpView>
{
    private readonly float MIN_X = -2.3f;
    private readonly float MAX_X = 2.3f;
    public readonly float INITIAL_Y = 5.5f;
    public readonly float MOVE_SPEED = -5.0f;
    
    private PowerUpConstant.PowerUpType _powerUpType;
    private bool _activate;

    public override void Start()
    {
        base.Start();
        ResetPosition();
    }

    private void ResetPosition()
    {
        _view.SetInitialPosition(_gameUtility.RandomRange(MIN_X, MAX_X), INITIAL_Y);
    }

    public void OnTriggerEntered(string tag)
    {
        if (tag == Tag.PLAYER)
        {
            PlaySound(() => _view.PlayPickSound());
            
            EventParam eventParam = EventParam.Create();
            eventParam.AddParam(EventParamConstant.POWER_UP_TYPE, _powerUpType);
            
            _view.TriggerEvent(EventConstant.POWER_UP_HIT_PLAYER, eventParam);
            _activate = false;
            ResetPosition();
        }
    }

    public void OnTriggerExited(string tag)
    {
        if (tag == Tag.BOUNDARY)
        {
            _activate = false;
            ResetPosition();
        }
    }

    public void UpdatePosition()
    {
        if (_activate)
        {
            _view.StartMoving(MOVE_SPEED);
        }
    }
    public void OnPowerUpActivated(PowerUpConstant.PowerUpType powerUpType)
    {
        _powerUpType = powerUpType;
        _activate = true;
        SetPowerUpSprite(powerUpType);
    }

    private void SetPowerUpSprite(PowerUpConstant.PowerUpType powerUpType)
    {
        switch (powerUpType)
        {
            case PowerUpConstant.PowerUpType.GunUpgrade:
                _view.SetGunUpgradeSprite();
                break;
            case PowerUpConstant.PowerUpType.Health:
                _view.SetHealthSprite();
                break;
            case PowerUpConstant.PowerUpType.Shield:
                _view.SetShieldSprite();
                break;
        }
    }
}
