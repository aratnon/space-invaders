﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BaseView : PoolableObject, IView
{
    [Inject] protected ResourceManager _resourceManager;
    [Inject] protected GameConstant _gameConstant;
    [Inject] protected UnityUtility _unityUtility;
    [Inject] protected EventManager _eventManager;
    [Inject] protected ObjectPoolManager _objectPoolManager;
    [Inject (Id = InjectionIds.Id.MainCamera)] protected Camera _mainCamera;

    protected AudioSource _audioSource;

    public virtual void Show()
    {
        gameObject.SetActive(true);
    }

    public virtual void Hide()
    {
        gameObject.SetActive(false);
    }

    public void TriggerEvent(string eventId, EventParam eventParam)
    {
        _eventManager.TriggerEvent(eventId, eventParam);
    }

    public void TriggerEvent(string eventId)
    {
        _eventManager.TriggerEvent(eventId);
    }

    protected void PlaySound(AudioClip clip, bool loop)
    {
        if (_audioSource == null) return;
        _audioSource.loop = loop;
        _audioSource.clip = clip;
        _audioSource.Play();
    }
}
