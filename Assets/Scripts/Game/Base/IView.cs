﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IView
{
    void Show();
    void Hide();

    void TriggerEvent(string eventId, EventParam eventParam);
    void TriggerEvent(string eventId);

}
