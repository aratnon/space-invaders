﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BaseController<TV> where TV: IView
{
    protected TV _view;
    [Inject] protected IGameUtility _gameUtility;
    [Inject] protected GameManager _gameManager;

    [Inject] protected SaveGameManager _saveGameManager;
    protected int PlayerGunLevel => _gameManager.PlayerModel.GunLevel;
    protected bool Playing => _gameManager.CurrentGameState == GameManager.GameState.Play;
    
    public TV View
    {
        set => _view = value;
    }

    public IGameUtility GameUtility
    {
        get => _gameUtility;
        set => _gameUtility = value;
    }

    public virtual void Start()
    {
        
    }

    protected void PlaySound(Action playAudioAction)
    {
        //if(_gameManager.SoundEnabled) ***For sound setting ***
        
        playAudioAction.Invoke();
    }
}
