﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : BaseController<IBulletView>
{
    public BulletModel BulletModel;
    public bool HasSpawned { get; set; }

    public void OnSpawned(BulletModel bulletModel)
    {
        HasSpawned = true;
        BulletModel = bulletModel;
    }

    public void UpdateBullet()
    {
        if (HasSpawned)
        {
            _view.Move(BulletModel.Speed);
        }
    }

    public void OnTriggerEntered(string colliderTag, string colliderName)
    {
        if (BulletModel.Source == Tag.ENEMY && colliderTag == Tag.SHIELD)
        {
            _view.Hide();
        }
        else if (colliderTag == Tag.PLAYER && BulletModel.Source == Tag.ENEMY ||
            colliderTag == Tag.ENEMY && BulletModel.Source == Tag.PLAYER)
        {
            var eventParam = CreateHitParam();
            var bulletHitEventTag = GetBulletHitEventTag(colliderTag, colliderName);
            _view.TriggerEvent(bulletHitEventTag, eventParam);
            _view.Hide();
        }
    }
    
    public void OnTriggerExited(string tag)
    {
        if (tag != Tag.BOUNDARY) return;
        _view.Hide();
    }

    public EventParam CreateHitParam()
    {
        var eventParam = EventParam.Create();
        eventParam.AddParam(EventParamConstant.BULLET_DAMAGE, BulletModel.Damage);

        return eventParam;
    }
    
    public string GetBulletHitEventTag(string tag, string colliderName)
    {
        return tag == Tag.ENEMY ? 
            EventConstant.GetEnemyBulletHitEventTag(colliderName) 
            : EventConstant.GetPlayerBulletHitEventTag();
    }
}
