﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBaseBulletSpawnerView: IView
{
    void SpawnBullet(BulletModel bulletModel);
}
