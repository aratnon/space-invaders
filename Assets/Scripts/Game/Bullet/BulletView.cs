﻿using System;
using UnityEngine;
using Zenject;

public class BulletView : BaseView, IBulletView
{
    [Inject] private BulletController _bulletController;
    
    public void Spawn(BulletModel bulletModel)
    {
        _bulletController.View = this;
        gameObject.SetActive(true);
        _bulletController.OnSpawned(bulletModel);
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        _bulletController.OnTriggerEntered(collision.tag, collision.name);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _bulletController.OnTriggerExited(collision.tag);
    }
    
    void Update()
    {
        _bulletController.UpdateBullet();
    }

    public void Move(float speed)
    {
        transform.Translate(Time.deltaTime * speed * Vector3.up); 
    }
}
