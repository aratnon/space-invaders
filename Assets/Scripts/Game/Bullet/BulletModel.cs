﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletModel
{
    public static BulletModel Create(
        int damage, 
        float speed,
        string source, 
        string bulletKey)
    {
        return new BulletModel
        {
            Damage = damage,
            Speed = speed,
            Source = source,
            BulletKey = bulletKey
        };
    }
    
    public string Source { get; set; }
    public int Damage { get; set; }
    public float Speed { get; set; }
    public string BulletKey { get; set; }

}
