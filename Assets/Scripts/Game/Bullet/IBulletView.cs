﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBulletView : IView
{
    void Move(float speed);
}
