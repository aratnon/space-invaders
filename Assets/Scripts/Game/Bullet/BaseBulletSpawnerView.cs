﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BaseBulletSpawnerView  : BaseView, IBaseBulletSpawnerView
{
    [Serializable]
    private class BulletSpawnerPoint
    {
        public Transform Transform;
        public ParticleSystem Particle;
    }
    
    [Inject] private BaseBulletSpanwerController _baseBulletController;
    [SerializeField] private List<BulletSpawnerPoint> spawnerPoints;
    
    public int Index { get; set; }
    private void Start()
    {
        _baseBulletController.View = this;
    }

    public void StartSpawnBullet(BulletModel bulletModel)
    {
        _baseBulletController.StartSpawnBullet(bulletModel);
    }
    
    public void SpawnBullet(BulletModel bulletModel)
    {
        spawnerPoints.ForEach(spawnPoint =>
        {
            BulletView bullet = _objectPoolManager.GetComponentFromPoolableObject<BulletView>(bulletModel.BulletKey);
            
            var bulletTransform = bullet.transform;
            bulletTransform.position = spawnPoint.Transform.position;
            bulletTransform.rotation = spawnPoint.Transform.rotation;

            if (spawnPoint.Particle != null)
            {
                spawnPoint.Particle.Play();
            }
            bullet.Spawn(bulletModel);
        });
    }
}
