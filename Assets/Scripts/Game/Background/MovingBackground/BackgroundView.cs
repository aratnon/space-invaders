﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BackgroundView : BaseView, IBackgroundView
{
    [Inject] private BackgroundController _backgroundController;
    void Start()
    {
        _backgroundController.View = this;
        _backgroundController.InitialBackgroundSpeed = _gameConstant.InitialBackgroundSpeed;
    }

    void Update()
    {
        _backgroundController.MoveBackground();
    }
    
    public void MoveBackground(float speed)
    {
        transform.Translate(Time.deltaTime * speed * Vector3.up); 
    }
}
