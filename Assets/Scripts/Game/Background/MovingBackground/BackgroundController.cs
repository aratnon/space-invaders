﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BackgroundController : BaseController<IBackgroundView>
{
    private float _initialBackgroundSpeed;
    
    public float InitialBackgroundSpeed
    {
        get => _initialBackgroundSpeed;
        set => _initialBackgroundSpeed = value;
    }

    public void MoveBackground()
    {
        if (Playing)
        {
            StartMovingBackground();
        }
//        else if (_gameManager.CurrentGameState == GameManager.GameState.Pause
//                 || _gameManager.CurrentGameState == GameManager.GameState.GameOver)
//        {
//            StopMovingBackground();
//        }
    }

    private void StartMovingBackground()
    {
        _view.MoveBackground(LevelModelFactory.GetGameSpeed(
            _gameManager.CurrentWaveNumber,
            _initialBackgroundSpeed));
    }

    private void StopMovingBackground()
    {
        _view.MoveBackground(0);
    }
    
}
