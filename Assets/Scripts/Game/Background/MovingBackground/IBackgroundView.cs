﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBackgroundView : IView
{
    void MoveBackground(float speed);
}
