﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Background Sprites")]
public class BackgroundResources : ScriptableObject
{
    [SerializeField] private List<Sprite> _backgroundSprites;

    public int Total => _backgroundSprites.Count;
    public Sprite GetBackground(int index)
    {
        return _backgroundSprites[index];
    }
}
