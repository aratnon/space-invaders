﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatingBackgroundController : BaseController<IRepeatingBackgroundView>
{
    private float _height;
    private int _totalBackground;

    public float Height
    {
        set => _height = value;
        get => _height;
    }

    public int TotalBackground
    {
        set => _totalBackground = value;
        get => _totalBackground;
    }

    public override void Start()
    {
        SetBackground();
    }

    public void UpdateOffset(float currentYPosition)
    {
        if (currentYPosition < -_height)
        {
            _view.RepositionBackground(_height);
            SetBackground();
        } 
    }

    private void SetBackground()
    {
        _view.SetBackground(_gameUtility.RandomRange(0, _totalBackground));
    }
}
