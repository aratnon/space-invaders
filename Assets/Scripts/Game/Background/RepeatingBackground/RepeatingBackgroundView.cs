﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(
    typeof(BoxCollider2D), 
    typeof(SpriteRenderer)
    )]
public class RepeatingBackgroundView : BaseView, IRepeatingBackgroundView
{
    [Inject] private RepeatingBackgroundController _repeatingBackgroundController;
    
    private BoxCollider2D _collider2D;
    private SpriteRenderer _spriteRenderer;

    private void Awake()
    {
        _collider2D = GetComponent<BoxCollider2D>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
    }
    
    void Start()
    {
        _repeatingBackgroundController.View = this;
        _repeatingBackgroundController.Height = _collider2D.size.y;
        _repeatingBackgroundController.TotalBackground = _resourceManager.BackgroundResources.Total;
        _repeatingBackgroundController.Start();
    }
    
    void Update()
    {
        _repeatingBackgroundController.UpdateOffset(transform.position.y);
    }

    public void RepositionBackground(float height)
    {
        Vector2 offset = new Vector2(0, height * 2f);
        transform.position = (Vector2) transform.position + offset;
    }
    
    public void SetBackground(int backgroundIndex)
    {
        _spriteRenderer.sprite = _resourceManager.BackgroundResources.GetBackground(backgroundIndex);
    }
}
