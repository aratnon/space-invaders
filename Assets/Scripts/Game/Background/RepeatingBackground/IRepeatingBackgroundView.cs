﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IRepeatingBackgroundView : IView
{
    void RepositionBackground(float height);
    void SetBackground(int backgroundIndex);
}
