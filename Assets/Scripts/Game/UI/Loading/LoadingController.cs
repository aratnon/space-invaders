﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LoadingController : BaseController<ILoadingView>
{
    public override void Start()
    {
        _saveGameManager.Init(false);
        _view.CreateObjectPool();
    }

    public void OnLoadingFinished()
    {
        _view.Hide();
    }
}
