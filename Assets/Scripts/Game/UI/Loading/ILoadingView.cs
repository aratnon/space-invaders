﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILoadingView : IView
{
    void CreateObjectPool();
}
