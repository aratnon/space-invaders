﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LoadingView : BaseView, ILoadingView
{
    [Inject] private LoadingController _loadingController;

    void Start()
    {
        _loadingController.View = this;
        _loadingController.Start();
    }

    public void CreateObjectPool()
    {
        _objectPoolManager.CreatePools(() =>
        {
            _loadingController.OnLoadingFinished();
        });
    }
}
