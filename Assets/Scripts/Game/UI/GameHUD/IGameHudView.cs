﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameHudView : IView
{
    void SetActiveHud(bool active);
    void UpdateGameScore(string score);
    void UpdateGold(string gold);
    void UpdatePlayerFuelBar(float percentage);

    void UpdatePlayerFuelBarPosition(EventParam positionParam);
}
