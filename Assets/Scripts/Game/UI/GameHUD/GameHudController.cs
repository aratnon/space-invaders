﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHudController : BaseController<IGameHudView>
{
    public override void Start()
    {
        _view.SetActiveHud(false);
    }

    public void StartGame()
    {
        UpdateGameScore();
        UpdateGold();
        UpdatePlayerFuelBar();
        _view.SetActiveHud(true);
    }

    public void GameOver()
    {
        _view.SetActiveHud(false);
    }

    public void UpdateGameScore()
    {
        _view.UpdateGameScore($"Score: {_gameManager.GameScore}");
    }

    public void UpdateGold()
    {
        _view.UpdateGold(_gameManager.Gold.ToString());
    }

    public void UpdatePlayerFuelBar()
    {
        _view.UpdatePlayerFuelBar(_gameManager.PlayerModel.HealthRatio);
    }
    
    public void UpdatePlayerFuelBarPosition(EventParam eventParam)
    {
        _view.UpdatePlayerFuelBarPosition(eventParam);
    }
}
