﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameHudView : BaseView, IGameHudView
{
    [Inject] private GameHudController _gameHudController;

    [SerializeField] private GameObject mainHud;
    [SerializeField] private Text scoreText;
    [SerializeField] private Text goldText;
    [SerializeField] private Image playerFuelBar;

    private Action<EventParam> _gameStartEvent;
    private Action<EventParam> _updateGameScoreEvent;
    private Action<EventParam> _updateGoldEvent;
    private Action<EventParam> _updatePlayerFuelBarEvent;
    private Action<EventParam> _updatePlayerFuelBarPositionEvent;
    private Action<EventParam> _gameOverEvent;
    
    void Start()
    {
        _gameHudController.View = this;
        _gameHudController.Start();
    }

    private void OnEnable()
    {
        _gameStartEvent = _eventManager.StartListening(EventConstant.GAME_START,
            param => { _gameHudController.StartGame(); });
        
        _updateGameScoreEvent = _eventManager.StartListening(EventConstant.UPDATE_GAME_SCORE,
            param => { _gameHudController.UpdateGameScore(); });
        
        _updateGoldEvent = _eventManager.StartListening(EventConstant.UPDATE_GOLD,
            param => { _gameHudController.UpdateGold(); });
        
        _updatePlayerFuelBarEvent = _eventManager.StartListening(EventConstant.UPDATE_PLAYER_FUEL_LEVEL,
            param => { _gameHudController.UpdatePlayerFuelBar(); });
        
        _updatePlayerFuelBarPositionEvent = _eventManager.StartListening(EventConstant.UPDATE_PLAYER_FUEL_BAR_POSITION,
            param => { _gameHudController.UpdatePlayerFuelBarPosition(param); });
        
        _gameOverEvent = _eventManager.StartListening(EventConstant.GAME_OVER,
            param => { _gameHudController.GameOver(); });
    }

    private void OnDestroy()
    {
        _eventManager.StopListening(EventConstant.GAME_START, _gameStartEvent);
        _eventManager.StopListening(EventConstant.UPDATE_GAME_SCORE, _updateGameScoreEvent);
        _eventManager.StopListening(EventConstant.UPDATE_GOLD, _updateGoldEvent);
        _eventManager.StopListening(EventConstant.UPDATE_PLAYER_FUEL_LEVEL, _updatePlayerFuelBarEvent);
        _eventManager.StopListening(EventConstant.UPDATE_PLAYER_FUEL_BAR_POSITION, _updatePlayerFuelBarPositionEvent);
        _eventManager.StopListening(EventConstant.GAME_OVER, _gameOverEvent);
    }

    public void SetActiveHud(bool active)
    {
        mainHud.SetActive(active);
    }

    public void UpdateGameScore(string score)
    {
        scoreText.text = score;
    }

    public void UpdateGold(string gold)
    {
        goldText.text = gold;
    }

    public void UpdatePlayerFuelBar(float percentage)
    {
        playerFuelBar.fillAmount = percentage;
    }

    public void UpdatePlayerFuelBarPosition(EventParam positionParam)
    {
        Vector3 playerPosition = positionParam.GetParam<Vector3>(EventParamConstant.PLAYER_POSITION);
        playerPosition.x += 0.75f;
        
        playerFuelBar.transform.position = playerPosition;
    }
}
