﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMainMenuView : IView
{
    void HideMainMenu();
}
