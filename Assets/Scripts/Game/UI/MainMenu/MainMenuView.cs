﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class MainMenuView : BaseView, IMainMenuView
{
    private readonly int _fadeOutTrigger = Animator.StringToHash("Fade Out");
    private readonly int _fadeInTrigger = Animator.StringToHash("Fade In");

    [Inject] private MainMenuController _mainMenuController;
    [Inject] private PowerUpDataManager _powerUpDataManager;
    
    private Animator _mainMenuAnimator;

    private void Awake()
    {
        _mainMenuAnimator = GetComponent<Animator>();
    }

    void Start()
    {
        _mainMenuController.View = this;
    }

    public void ClickStart()
    {
        _mainMenuController.OnClickedStart();
    }

    public void ClickShop()
    {
        _mainMenuController.OnClickedShop();
    }
    
    public void HideMainMenu()
    {
        _mainMenuAnimator.SetTrigger(_fadeOutTrigger);
    }

    public void FinishFadeOutAnimation()
    {
        _mainMenuController.StartGame();
    }
}
