﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : BaseController<IMainMenuView>
{
    public override void Start()
    {
        _view.Show();
    }

    public void OnClickedStart()
    {
        _view.HideMainMenu();
    }

    public void OnClickedShop()
    {
        _view.TriggerEvent(EventConstant.SHOW_SHOP_SCREEN);
    }

    public void StartGame()
    {
        _view.TriggerEvent(EventConstant.CLICK_GAME_START);
        _view.Hide();
        
    }
}
