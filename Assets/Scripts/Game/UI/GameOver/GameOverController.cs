﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOverController : BaseController<IGameOverView>
{
    public override void Start()
    {
        _view.SetActiveMainPanel(false);
    }

    public void ShowGameOverScreen()
    {
        _view.SetGoldEarned($"Gold Earned: {_gameManager.CurrentGameModel.GoldEarned}");
        _view.SetScoreEarned($"Score Earned: {_gameManager.CurrentGameModel.GameScore}");
        _view.SetActiveMainPanel(true);
    }

    public void OnClickedShop()
    {
        _view.TriggerEvent(EventConstant.SHOW_SHOP_SCREEN);
    }

    public void OnClickedRestart()
    {
        _view.TriggerEvent(EventConstant.CLICK_GAME_START);
    }
}
