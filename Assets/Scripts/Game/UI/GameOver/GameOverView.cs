﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class GameOverView : BaseView, IGameOverView
{
    [Inject] private GameOverController _gameOverController;

    [SerializeField] private GameObject mainPanel;
    [SerializeField] private Text scoreEarnedText;
    [SerializeField] private Text goldEarnedText;

    private Action<EventParam> _gameOverEvent;

    private void Start()
    {
        _gameOverController.View = this;
        _gameOverController.Start();
    }

    private void OnEnable()
    {
        _gameOverEvent = _eventManager.StartListening(EventConstant.GAME_OVER, 
            param => { _gameOverController.ShowGameOverScreen(); });
    }

    private void OnDisable()
    {
        _eventManager.StopListening(EventConstant.GAME_OVER, _gameOverEvent);
    }

    public void ClickShop()
    {
        _gameOverController.OnClickedShop();
    }

    public void ClickRestart()
    {
        _gameOverController.OnClickedRestart();
    }

    public void SetActiveMainPanel(bool active)
    {
        mainPanel.SetActive(active);
    }

    public void SetScoreEarned(string score)
    {
        scoreEarnedText.text = score;
    }

    public void SetGoldEarned(string gold)
    {
        goldEarnedText.text = gold;
    }
}
