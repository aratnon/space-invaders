﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGameOverView : IView
{
    void SetActiveMainPanel(bool active);
    void SetScoreEarned(string score);
    void SetGoldEarned(string gold);
}
