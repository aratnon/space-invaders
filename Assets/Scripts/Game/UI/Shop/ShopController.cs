﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopController : BaseController<IShopView>
{
    private List<BasePowerUpModel> _powerUpModels;

    public void Start(List<BasePowerUpModel> powerUpModels)
    {
        _powerUpModels = powerUpModels;
        
        CreateShopItem();
    }
    private void CreateShopItem()
    {
        for (int i = 0; i != _powerUpModels.Count; ++i)
        {
            _view.CreateShopItem(i);
        }
        _view.UpdateListHeight();
        _view.SetActiveMainPanel(false);
    }

    public void Show()
    {
        UpdateShopItemLevel();
        UpdateGold();
        _view.SetActiveMainPanel(true);
    }

    private void UpdateGold()
    {
        _view.UpdateGold(_saveGameManager.SaveGame.Gold.ToString());
    }

    public void OnClickedClose()
    {
        _saveGameManager.Save();
        _view.SetActiveMainPanel(false);
    }
    
    public void UpdateShopItemLevel()
    {
        var saveGame = _saveGameManager.SaveGame;
        for (int i = 0; i != _powerUpModels.Count; ++i)
        {
            var model = _powerUpModels[i];
            model.CurrentLevel = saveGame.GetPowerUpLevel(model.Key);
            var enoughGold = IsGoldEnough(model, saveGame);
            
            _view.UpdateShopItem(i, enoughGold, model);
        }
        _view.UpdateListHeight();
    }

    public void UpgradePowerUp(int index)
    {
        var saveGame = _saveGameManager.SaveGame;
        var model = _powerUpModels[index];
        
        saveGame.ModifyGold(-model.CurrentUpgradeCost);
        saveGame.UpgradePowerUp(model.Key, model.MaxLevel);
        
        model.CurrentLevel = saveGame.GetPowerUpLevel(model.Key);
        
        var enoughGold = IsGoldEnough(model, saveGame);
        
        UpdateGold();
        
        PlaySound(() => _view.PlaySpendGoldSound());
        _view.UpdateShopItem(index, enoughGold, model);
        _view.UpdateListHeight();
    }

    public bool IsGoldEnough(BasePowerUpModel model, SaveGame saveGame)
    {
        return model.IsMaxLevel || model.CurrentUpgradeCost <= saveGame.Gold;
    }
}
