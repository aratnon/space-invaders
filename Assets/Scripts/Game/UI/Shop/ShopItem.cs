﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    public Image icon;
    public Text nameText;
    public Text levelText;
    public Text descriptionText;
    public Text nextValueText;
    public Text upgradeCostText;
    public Button upgradeButton;
}
