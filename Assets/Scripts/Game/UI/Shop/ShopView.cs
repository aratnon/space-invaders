﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ShopView : BaseView, IShopView
{
    [Inject] private ShopController _shopController;
    [Inject] private PowerUpDataManager _powerUpDataManager;

    [SerializeField] private GameObject mainPanel;
    [SerializeField] private Text goldText;
    [SerializeField] private ShopItem shopItemPrefab;
    [SerializeField] private VerticalLayoutGroup shopItemParent;
    [SerializeField] private AudioClip spendGoldSound;
    
    private List<ShopItem> _shopItems = new List<ShopItem>();

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _shopController.View = this;
        _shopController.Start(_powerUpDataManager.CreatePowerUpModels());
    }

    private void OnEnable()
    {
        _eventManager.StartListening(EventConstant.SHOW_SHOP_SCREEN, 
            param => { _shopController.Show(); });
    }

    public void ClickClose()
    {
        _shopController.OnClickedClose();
    }

    public void PlaySpendGoldSound()
    {
        PlaySound(spendGoldSound, false);
    }

    public void SetActiveMainPanel(bool active)
    {
        mainPanel.SetActive(active);
    }
    

    public void UpdateGold(string gold)
    {
        goldText.text = gold;
    }

    public void CreateShopItem(int index)
    {
        var shopItem = Instantiate(shopItemPrefab, shopItemParent.transform);
        shopItem.upgradeButton.onClick.AddListener(() => { _shopController.UpgradePowerUp(index); });
        _shopItems.Add(shopItem);
    }

    public void UpdateListHeight()
    {
        Canvas.ForceUpdateCanvases();
        shopItemParent.SetLayoutVertical();
    }

    public void UpdateShopItem(int index, bool enoughGold, BasePowerUpModel model)
    {
        var shopItem = _shopItems[index];
        shopItem.icon.sprite = _powerUpDataManager.GetIcon(model.Key);
        shopItem.nameText.text = model.GetName();
        shopItem.levelText.text = model.LevelString;
        shopItem.descriptionText.text = model.GetDescription();
        shopItem.nextValueText.text = model.GetNextLevelValue();

        if (!model.IsMaxLevel)
        {
            shopItem.upgradeCostText.text = model.CurrentUpgradeCost.ToString();
            shopItem.upgradeButton.interactable = enoughGold;
        }
        
        shopItem.upgradeButton.gameObject.SetActive(!model.IsMaxLevel);
    }
}

