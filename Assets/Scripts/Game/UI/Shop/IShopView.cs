﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShopView : IView
{
    void PlaySpendGoldSound();
    void SetActiveMainPanel(bool active);
    void UpdateGold(string gold);
    void CreateShopItem(int index);

    void UpdateListHeight();
    void UpdateShopItem(int index, bool enoughGold, BasePowerUpModel model);


}
