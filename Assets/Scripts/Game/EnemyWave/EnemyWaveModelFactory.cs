﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveModelFactory: BaseModelFactory
{
    private static readonly int BASE_GOLD_DROP = 10;
    private static readonly int BASE_GOLD_INCREASE = 5;
    private static readonly int BASE_ENEMY_SCORE = 5;
    private static readonly int BASE_ENEMY_SCORE_INCREASE = 3;
    private static readonly float BASE_MOVE_SPEED = 4.0f;
    private static readonly float BASE_MOVE_SPEED_INCREASE = 0.25f;

    public static EnemyWaveModel CreateEnemyWaveModel(
        int waveNumber,
        int totalEnemy,
        float delayBetweenEachEnemy
    )
    {
        return new EnemyWaveModel
        {
            WaveNumber = waveNumber,
            TotalEnemy = totalEnemy,
            DelayBetweenEachEnemy = delayBetweenEachEnemy,
            GoldDrop = GetGoldDrop(waveNumber),
            EnemyScore = GetEnemyScore(waveNumber),
            MoveSpeed = GetMoveSpeed(waveNumber)
        };
    }

    private static float GetMoveSpeed(int waveNumber)
    {
        return GetIncreasingValue(waveNumber, BASE_MOVE_SPEED, BASE_MOVE_SPEED_INCREASE);
    }
    
    private static int GetGoldDrop(int waveNumber)
    {
        return Mathf.RoundToInt(GetIncreasingValue(waveNumber, BASE_GOLD_DROP, BASE_GOLD_INCREASE));
    }

    private static int GetEnemyScore(int waveNumber)
    {
        return Mathf.RoundToInt(GetIncreasingValue(waveNumber, BASE_ENEMY_SCORE, BASE_ENEMY_SCORE_INCREASE));
    }
}
