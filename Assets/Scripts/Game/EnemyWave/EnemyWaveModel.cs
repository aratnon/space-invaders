﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveModel
{
    public static readonly string ENEMY_WAVE_PARAM_KEY = "ENEMY_WAVE_PARAM";
    public static readonly string WAVE_NUMBER = "WAVE_NUMBER";
    
    public int WaveNumber { get; set; }
    public int TotalEnemy { get; set; }
    public int GoldDrop { get; set; }
    
    public int EnemyScore { get; set; }
    public float DelayBetweenEachEnemy { get; set; }
    public float MoveSpeed { get; set; }
}
