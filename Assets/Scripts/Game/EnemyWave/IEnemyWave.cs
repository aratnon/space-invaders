﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IEnemyWave : IView
{
    void CreateEnemy(int enemyIndex, EnemyWaveModel enemyWaveModel);
}
