﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWaveController : BaseController<IEnemyWave>
{
    private int _currentEnemyIndex;
    private int _finishedEnemyCount;
    private float _currentDelayCounter;
    private EnemyWaveModel _enemyWaveModel;
    public void Start(EnemyWaveModel enemyWaveModel)
    {
        _enemyWaveModel = enemyWaveModel;
        _currentEnemyIndex = 0;
        _finishedEnemyCount = 0;
    }

    public void UpdateCreatingEnemy(float dt)
    {
        if (!IsWaveFinished())
        {
            _currentDelayCounter += dt;
            if (_currentDelayCounter >= _enemyWaveModel.DelayBetweenEachEnemy)
            {
                _currentDelayCounter = 0;
                _view.CreateEnemy(_currentEnemyIndex, _enemyWaveModel);
            }
        }
    }

    public void OnEnemyCreated()
    {
        _currentEnemyIndex += 1;
    }

    private bool IsWaveFinished()
    {
        return _currentEnemyIndex == _enemyWaveModel.TotalEnemy;
    }
    
    public void IncreaseFinishedEnemyCount()
    {
        if (Playing)
        {
            _finishedEnemyCount += 1;
            if (_finishedEnemyCount == _enemyWaveModel.TotalEnemy)
            {
                _finishedEnemyCount = 0;
                _view.TriggerEvent(EventConstant.FINISH_WAVE);
            } 
        }
    }
}
