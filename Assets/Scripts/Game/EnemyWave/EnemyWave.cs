﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;



public class EnemyWave : BaseView, IEnemyWave
{
    [Serializable]
    public class PathSetting
    {
        public Transform Path;
        public bool CanFire;
        
    }
    
    [Inject] private EnemyWaveController _enemyWaveController;
    
    [SerializeField] private List<PathSetting> paths;

    [SerializeField] private List<string> enemyId;

    [SerializeField] private float delayBetweenEachEnemy;

    private Action<EventParam> _finishedEnemyEvent;

    private void Start()
    {
        _enemyWaveController.View = this;
        _finishedEnemyEvent = _eventManager.StartListening(EventConstant.FINISHED_ENEMY,
            param => { _enemyWaveController.IncreaseFinishedEnemyCount(); });
    }

    public void StartCreatingEnemy(int waveNumber)
    {
        EnemyWaveModel enemyWaveModel = EnemyWaveModelFactory.CreateEnemyWaveModel(
            waveNumber,
            enemyId.Count,
            delayBetweenEachEnemy
        );
        _enemyWaveController.Start(enemyWaveModel);
    }

    private void OnDestroy()
    {
        _eventManager.StopListening(EventConstant.FINISHED_ENEMY, _finishedEnemyEvent);
    }

    private void Update()
    {
        _enemyWaveController.UpdateCreatingEnemy(Time.deltaTime);
    }

    public void CreateEnemy(int enemyIndex, EnemyWaveModel enemyWaveModel)
    {
        EnemyView enemyView = _objectPoolManager.GetComponentFromPoolableObject<EnemyView>(enemyId[enemyIndex]);
        EnemyModel enemyModel = EnemyModelFactory.CreateEnemyModel(enemyIndex, enemyWaveModel);
        enemyView.Spawn(enemyModel, paths);
        
        _enemyWaveController.OnEnemyCreated();
    }
}
