﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerModel: BaseUnitModel
{
    private static readonly int BASE_FUEL_LEVEL = 30;
    public static readonly int BASE_FUEL_DECREASE = 1;
    public static readonly int MAX_GUN_LEVEL = 4;
    public static readonly float MIN_SHIELD_TIME = 10;
    public static PlayerModel Create(int fuelRestorePoint, 
        int plasmaShieldTime)
    {
        var playerModel = new PlayerModel
        {
            GunLevel = 0,
            FuelRestorePoint = fuelRestorePoint,
            MaxPlasmaShieldTime = plasmaShieldTime,
            UpdateFuelTick = 0,
            MaxHealth = BASE_FUEL_LEVEL,
            PlasmaShieldActive = false
        };

        playerModel.CurrentHealth = playerModel.MaxHealth;
        return playerModel;
    }
    public int GunLevel { get; set; }
    public int FuelRestorePoint { get; set; }
    public int MaxPlasmaShieldTime { get; set; }    
    public float UpdateFuelTick { get; set; }
    public float CurrentPlasmaShieldTime { get; set; }
    public bool PlasmaShieldActive { get; set; }
    public void IncreaseGunLevel()
    {
        GunLevel = Mathf.Clamp(GunLevel + 1, 0, MAX_GUN_LEVEL);
    }
}
