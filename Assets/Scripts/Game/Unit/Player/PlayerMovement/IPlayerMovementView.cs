﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerMovementView : IView
{
    void MoveToTouchLocation();
    void TriggerUpdatePlayerPosition();
}
