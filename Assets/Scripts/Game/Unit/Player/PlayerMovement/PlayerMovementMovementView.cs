﻿using UnityEngine;
using Zenject;

public class PlayerMovementMovementView : BaseView, IPlayerMovementView
{
    [Inject] private PlayerMovementController _playerMovementController;
    private EventParam _playerPositionParam;
    void Start()
    {
        _playerMovementController.View = this;
        _playerPositionParam = EventParam.Create();
        _playerMovementController.Start();
    }

    private void Update()
    {
#if UNITY_EDITOR //if the current platform is not mobile, setting mouse handling 

        if (Input.GetMouseButton(0)) //if mouse button was pressed       
        {
            _playerMovementController.OnDragPlayer();
        }
#endif

#if UNITY_IOS || UNITY_ANDROID //if current platform is mobile, 
        if (Input.touchCount > 0) // if there is a touch
        {
            _playerMovementController.OnDragPlayer();
        }
#endif

    }

    public void MoveToTouchLocation()
    {
#if UNITY_EDITOR
        Vector3 mousePosition = _mainCamera.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = transform.position.z;
        transform.position = Vector3.MoveTowards(transform.position, mousePosition, 30 * Time.deltaTime);
#endif
        
#if UNITY_IOS || UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            Vector3 touchPosition = _mainCamera.ScreenToWorldPoint(touch.position); 
            touchPosition.z = transform.position.z;
            transform.position = Vector3.MoveTowards(transform.position, touchPosition, 30 * Time.deltaTime);
        }
#endif

        TriggerUpdatePlayerPosition();
    }

    public void TriggerUpdatePlayerPosition()
    {
        _playerPositionParam.AddParam(EventParamConstant.PLAYER_POSITION, transform.position);
        _eventManager.TriggerEvent(EventConstant.UPDATE_PLAYER_FUEL_BAR_POSITION, _playerPositionParam);
    }
}
