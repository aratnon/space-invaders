﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementController: BaseController<IPlayerMovementView>
{
    public override void Start()
    {
        _view.TriggerUpdatePlayerPosition();
    }

    public void OnDragPlayer()
    {
        if (Playing)
        {
            _view.MoveToTouchLocation(); 
            _view.TriggerUpdatePlayerPosition();
        }
    }
}
