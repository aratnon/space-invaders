﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : BaseUnitController<PlayerModel, IPlayerView>
{
    public override void Start()
    {
        _view.SetActiveShield(false);
    }

    public void StartGame()
    {
        UnitModel = _gameManager.PlayerModel;
    }
    
    public override void OnHitByBullet(EventParam eventParam)
    {
        base.OnHitByBullet(eventParam);
        _view.TriggerEvent(EventConstant.UPDATE_PLAYER_FUEL_LEVEL);
    }
    
    public void OnHitByPowerUp(PowerUpConstant.PowerUpType type)
    {
        switch (type)
        {
            case PowerUpConstant.PowerUpType.GunUpgrade:
                _gameManager.IncreasePlayerGunLevel();
                break;
            case PowerUpConstant.PowerUpType.Health:
                _gameManager.AddPlayerHealth();
                _view.TriggerEvent(EventConstant.UPDATE_PLAYER_FUEL_LEVEL);
                break;
            case PowerUpConstant.PowerUpType.Shield:
                _gameManager.ActivatePlayerShield();
                _view.SetActiveShield(true);
                break;
        }
    }

    public void UpdatePlayerFuel(float dt)
    {
        if (!Playing) return;
        _gameManager.UpdatePlayerFuelTick(dt);
        
        if (UnitModel.UpdateFuelTick < 1.0f) return;
        _gameManager.ResetPlayerFuelTick();
        TakeDamage(PlayerModel.BASE_FUEL_DECREASE);
        _view.TriggerEvent(EventConstant.UPDATE_PLAYER_FUEL_LEVEL);
    }

    public override void OnDied()
    {
        base.OnDied();
        _gameManager.GameOver();
        _view.Hide();
    }

    public void OnShieldDeactivated()
    {
        _view.SetActiveShield(false);
    }
}
