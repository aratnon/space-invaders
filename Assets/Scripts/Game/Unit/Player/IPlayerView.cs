﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlayerView : IUnitView
{
    void SetActiveShield(bool active);
}
