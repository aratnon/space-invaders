﻿using System;
using UnityEngine;
using Zenject;

public class PlayerView : BaseUnitView, IPlayerView
{
    private readonly string EXPLOSION_PARTICLE_ID = "Explosion";
    
    [Inject] private PlayerController _playerController;
    [SerializeField] private GameObject shield;

    private Action<EventParam> _startGrameEvent;
    private Action<EventParam> _powerUpHitEvent;
    private Action<EventParam> _shieldDeactivatedEvent;
    
    private void Start()
    {
        _playerController.View = this;
        _playerController.Start();
    }

    private void OnEnable()
    {
        StartListeningBulletHitEvent(param => _playerController.OnHitByBullet(param));
        _startGrameEvent = _eventManager.StartListening(EventConstant.GAME_START,
            param =>
            {
                BaseGunView.SetupGun();
                _playerController.StartGame();
            });
        
        _powerUpHitEvent = _eventManager.StartListening(EventConstant.POWER_UP_HIT_PLAYER,
            param =>
            {
                _playerController.OnHitByPowerUp(
                    param.GetParam<PowerUpConstant.PowerUpType>(EventParamConstant.POWER_UP_TYPE));
            });
        _shieldDeactivatedEvent = _eventManager.StartListening(EventConstant.DEACTIVATE_PLASMA_SHIELD,
            param => { _playerController.OnShieldDeactivated(); });
    }

    private void OnDisable()
    {
        _eventManager.StopListening(EventConstant.POWER_UP_HIT_PLAYER, _powerUpHitEvent);
        _eventManager.StopListening(EventConstant.DEACTIVATE_PLASMA_SHIELD, _shieldDeactivatedEvent);
    }

    private void Update()
    {
        _playerController.UpdatePlayerFuel(Time.deltaTime);
    }

    protected override string GetBulletHitEventTag()
    {
        return EventConstant.GetPlayerBulletHitEventTag();
    }
    
    protected override string GetDieExplosionParticleId()
    {
        return EXPLOSION_PARTICLE_ID;
    }

    public void SetActiveShield(bool active)
    {
        shield.SetActive(active);
    }
}
