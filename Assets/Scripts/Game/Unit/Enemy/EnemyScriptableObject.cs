﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "New Enemy")]
public class EnemyScriptableObject : ScriptableObject
{
    [SerializeField] private Sprite _sprite;
    [SerializeField] private BulletView _bullet;
}
