﻿using System.Numerics;
using Zenject;

public interface IEnemyView : IUnitView
{
    void SetInitialPosition();
    void MoveAlongPath(int pathIndex, float moveSpeed);
    void Fire();
    
    void Spawn();
    void Destroy();
}
