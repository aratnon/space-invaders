﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameModel
{
    public static readonly float SCORE_INCREASE_TICK_RATE = 0.5f;
    public static readonly float SCORE_INCREASE_RATE = 1f;
    
    public int WaveNumber { get; set; }
    public float GameScore { get; set; }
    public float PowerUpTime { get; set; }
    public float ScoreTickCount { get; set; }
    
    public int GoldEarned { get; set; }

    public static GameModel Create()
    {
        return new GameModel
        {
            WaveNumber = 0,
            GameScore = 0,
            PowerUpTime = 0,
            ScoreTickCount = 0
        };
    }
}
