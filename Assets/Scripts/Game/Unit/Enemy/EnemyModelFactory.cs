﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyModelFactory: BaseModelFactory
{
    private static readonly int BASE_ENEMY_HEALTH = 1;
    private static readonly int BASE_ENEMY_HEALTH_INCREASE = 1;
    public static EnemyModel CreateEnemyModel(int enemyIndex, EnemyWaveModel enemyWaveModel)
    {
        EnemyModel enemyModel = new EnemyModel
        {
//            MaxHealth = GetHealth(enemyWaveModel.WaveNumber),
            MaxHealth = 1,
            MoveSpeed = enemyWaveModel.MoveSpeed,
            EnemyScore = enemyWaveModel.EnemyScore,
            UnitId = GetEnemyId(enemyIndex),
            CurrentPathIndex = 0,
            Moving = true
        };
        enemyModel.CurrentHealth = enemyModel.MaxHealth;
        return enemyModel;
    }

    private static string GetEnemyId(int enemyIndex)
    {
        return $"ENEMY_{enemyIndex}";
    }
    private static int GetHealth(int waveNumber)
    {
        return Mathf.RoundToInt(GetIncreasingValue(waveNumber, BASE_ENEMY_HEALTH, BASE_ENEMY_HEALTH_INCREASE));
    }
}
