﻿using System.Collections.Generic;

public class EnemyModel: BaseUnitModel
{
    public EnemyConstant.EnemyType EnemyType { get; set; }
    public float MoveSpeed { get; set; }
    public int EnemyScore { get; set; }
    
    public int CurrentPathIndex { get; set; }
    
    public int TotalPath { get; set; }
    
    public bool Moving { get; set; }
    
    public List<int> FiringLocations { get; set; }

}
