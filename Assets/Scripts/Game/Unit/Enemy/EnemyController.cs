﻿using System.Collections.Generic;
using UnityEngine;

public class EnemyController : BaseUnitController<EnemyModel, IEnemyView>
{
    public void Start(EnemyModel enemyModel)
    {
        UnitModel = enemyModel;
        _view.SetInitialPosition();
    }

    public void UpdateMoving()
    {
        if (!UnitModel.Moving && UnitModel.IsDead()) return;
        
        _view.MoveAlongPath(UnitModel.CurrentPathIndex, UnitModel.MoveSpeed);
    }

    private void TriggerFinishedEvent()
    {
        _view.TriggerEvent(EventConstant.FINISHED_ENEMY);
    }
    
    private void TriggerEnemyDieEvent()
    {
        EventParam eventParam = EventParam.Create();
        eventParam.AddParam(EventParamConstant.GAME_SCORE, UnitModel.EnemyScore);
        eventParam.AddParam(EventParamConstant.ENEMY_POSITION, _view.GetPosition());
        
        _view.TriggerEvent(EventConstant.ENEMY_DIE, eventParam);
    }

    public void OnReachedNextPath()
    {
        if (UnitModel.CurrentPathIndex >= UnitModel.TotalPath) return;
        if (UnitModel.FiringLocations.Contains(UnitModel.CurrentPathIndex))
        {
            _view.Fire();
        }
            
        UnitModel.CurrentPathIndex += 1;
        UnitModel.Moving = UnitModel.CurrentPathIndex < UnitModel.TotalPath;
        
        if (UnitModel.Moving) return;
        TriggerFinishedEvent();
        _view.StopListeningEvent();
        _view.Hide();
    }

    public override void OnDied()
    {
        base.OnDied();
        TriggerFinishedEvent();
        TriggerEnemyDieEvent();
        _view.Hide();
    }
}
