﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class EnemyView : BaseUnitView, IEnemyView
{
    private readonly string EXPLOSION_PARTICLE_ID = "Explosion";
    
    private class PathSetting
    {
        public Vector3 Position { get; set; }
        public bool CanFire { get; set; }
    }
    
    
    [Inject] private EnemyController _enemyController;
    
    private List<PathSetting> _pathSettings;

    protected override string GetBulletHitEventTag()
    {
        return EventConstant.GetEnemyBulletHitEventTag(name);
    }

    protected override string GetDieExplosionParticleId()
    {
        return EXPLOSION_PARTICLE_ID;
    }

    public void Spawn(EnemyModel enemyModel, List<EnemyWave.PathSetting> paths)
    {
        BaseGunView.SetupGun();
        
        CopyPaths(paths);
        name = enemyModel.UnitId;
        enemyModel.TotalPath = _pathSettings.Count;
        enemyModel.FiringLocations = GetFiringLocation();
        _enemyController.View = this;
        _enemyController.Start(enemyModel);
        StartListeningBulletHitEvent(param => _enemyController.OnHitByBullet(param));
    }

    private void CopyPaths(List<EnemyWave.PathSetting> pathSettings)
    {
        _pathSettings = new List<PathSetting>();
        pathSettings.ForEach(pathSetting =>
        {
            _pathSettings.Add(CopyPath(pathSetting));
        });
    }

    private PathSetting CopyPath(EnemyWave.PathSetting pathSetting)
    {
        var position = pathSetting.Path.position;
        return new PathSetting
        {
            Position = new Vector3(position.x, position.y, 0),
            CanFire = pathSetting.CanFire
        };
    }

    private List<int> GetFiringLocation()
    {
        var firingLocation = new List<int>();
        for (var i = 0; i != _pathSettings.Count; ++i)
        {
            if (_pathSettings[i].CanFire)
            {
                firingLocation.Add(i);
            }
        }

        return firingLocation;
    }

    private void Update()
    {
        _enemyController.UpdateMoving();
    }
    
//    void OnTriggerEnter2D(Collider2D collision)
//    {
//        _enemyController.OnTriggerEntered(collision.tag,
//            collision.name);
//    }

    public void SetInitialPosition()
    {
        transform.position = _pathSettings[0].Position;
    }

    public void MoveAlongPath(int pathIndex, float moveSpeed)
    {
        Vector3 pathPosition = _pathSettings[pathIndex].Position;
        transform.position = Vector2.MoveTowards(
            transform.position,
            pathPosition, 
            moveSpeed * Time.deltaTime
            );
        
        if (transform.position == pathPosition)
        {
            _enemyController.OnReachedNextPath();
        }
    }
    
    public void Fire()
    {
        BaseGunView.Fire();
    }

    public void Spawn()
    {
        throw new System.NotImplementedException();
    }

    public void Destroy()
    {
        throw new System.NotImplementedException();
    }
}
