﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IUnitView : IView
{
    Vector2 GetPosition();
    void StopListeningEvent();
    void PlayDieAnimation();
}
