﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseUnitModel
{
    public static readonly string TagEnemy = "Enemy";
    public static readonly string TagPlayer = "Player";

    public int Damage { get; set; }
    public int CurrentHealth { get; set; }
    public int MaxHealth { get; set; }
    
    public string UnitTag { get; set; }
    
    public string UnitId { get; set; }

    public bool IsDead() { return CurrentHealth <= 0; }

    public float HealthRatio => (float) CurrentHealth / (float) MaxHealth;

    public void ModifyCurrentHealth(int value)
    {
        CurrentHealth = Mathf.Clamp(CurrentHealth + value, 0, MaxHealth);
    }
}
