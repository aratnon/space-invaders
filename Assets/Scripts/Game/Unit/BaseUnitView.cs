﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUnitView : BaseView, IUnitView
{
    private Action<EventParam> _bulletHitEvent;
    public BaseGunView BaseGunView { get; set; }

    void Awake()
    {
        BaseGunView = GetComponent<BaseGunView>();
    }
    protected abstract string GetBulletHitEventTag();
    protected abstract string GetDieExplosionParticleId();

    protected void StartListeningBulletHitEvent(Action<EventParam> callback)
    {
        _bulletHitEvent = callback;
        _eventManager.StartListening(GetBulletHitEventTag(), _bulletHitEvent);
    }

    public Vector2 GetPosition()
    {
        return transform.position;
    }

    public void StopListeningEvent()
    {
        _eventManager.StopListening(GetBulletHitEventTag(), _bulletHitEvent);
    }

    public void PlayDieAnimation()
    {
        GameObject particle = _objectPoolManager.GetPoolableObject(GetDieExplosionParticleId());
        particle.transform.position = transform.position;
        particle.GetComponent<ParticleController>().PlayParticle().StopWhenFinishing();
    }
}
