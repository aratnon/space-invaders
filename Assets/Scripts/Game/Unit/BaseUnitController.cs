﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseUnitController<T, TV>: BaseController<TV> where TV: IUnitView where T: BaseUnitModel
{
    public T UnitModel { get; set; }
    
    public virtual void OnHitByBullet(EventParam eventParam)
    {
        if (Playing)
        {
            int damage = eventParam.GetParam<int>(EventParamConstant.BULLET_DAMAGE);
            TakeDamage(damage);
        }
    }

    protected void TakeDamage(int damage)
    {
        UnitModel.ModifyCurrentHealth(-damage);
        if (UnitModel.IsDead())
        {
            OnDied();
        }
    }

    public virtual void OnDied()
    {
        _view.StopListeningEvent();
        _view.PlayDieAnimation();
    }
}
