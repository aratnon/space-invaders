﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class BaseGunView : BaseView, IBaseGunView
{
    [Inject] private BaseGunController _baseGunController;
    [Inject] private GunModelFactory _gunModelFactory;

    [SerializeField] private AudioClip spawnSound;
    [SerializeField] private List<GunData> gunData;
    [SerializeField] private List<BaseBulletSpawnerView> _bulletSpawners;
    
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _baseGunController.View = this;
    }

    public void SetupGun()
    {
        _baseGunController.GunModels = _gunModelFactory.CreateGunModels(gunData);
        _baseGunController.Source = tag;
        _baseGunController.SetUpGun();
    }

    public void Fire()
    {
        _baseGunController.SpawnBullet();
    }
    
    private void Update()
    {
        _baseGunController.UpdateGunDelay(Time.deltaTime);
    }

    public void SpawnBullet(int gunLevel, BulletModel bulletModel)
    {
        _bulletSpawners[gunLevel].StartSpawnBullet(bulletModel);
    }

    public void PlaySpawnBulletSound()
    {
        PlaySound(spawnSound, false);
    }
}
