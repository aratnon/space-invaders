﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseGunController : BaseController<IBaseGunView>
{
    private List<GunModel> _gunModels;
    private GunModel _currentGunModel;
    private string _source;
    private float _delayCounter;
    private bool alreadySetup;
    public string Source
    {
        set => _source = value;
    }

    public List<GunModel> GunModels
    {
        set => _gunModels = value;
    }
    
    public void SetUpGun()
    {
        _delayCounter = 0;
        alreadySetup = true;
        int gunLevel = GetGunLevel();
        _currentGunModel = _gunModels[gunLevel];
    }

    public void UpdateGunDelay(float dt)
    {
        if (Playing && alreadySetup && _currentGunModel.IsCooldownBased)
        {
            _delayCounter += dt;
            if (_delayCounter >= _currentGunModel.GunDelay)
            {
                _delayCounter = 0;
                SpawnBullet();
            }
        }
    }

    private int GetGunLevel()
    {
        return _source == Tag.PLAYER ? PlayerGunLevel : 0;
    }

    public void SpawnBullet()
    {
        PlaySound(() => _view.PlaySpawnBulletSound());
        _view.SpawnBullet(GetGunLevel(), CreateBulletModel());
    }
    
    private BulletModel CreateBulletModel()
    {
        return BulletBaseModelFactory.CreateEnemyBulletModel(
            _gameManager.CurrentWaveNumber,
            _currentGunModel.Damage,
            _currentGunModel.BulletSpeed,
            _source,
            _currentGunModel.BulletKey
        );
    }
}
