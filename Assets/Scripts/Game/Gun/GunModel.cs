﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunModel
{
    public static GunModel Create(
        float gunDelay,
        int damage,
        float bulletSpeed,
        string bulletKey,
        bool isCooldownBased)
    {
        return new GunModel
        {
            GunDelay = gunDelay,
            Damage = damage,
            BulletSpeed = bulletSpeed,
            BulletKey = bulletKey,
            IsCooldownBased = isCooldownBased
        };
    }
    
    public float GunDelay { get; set; }
    public int Damage { get; set; }
    public float BulletSpeed { get; set; }
    public string BulletKey { get; set; }
    public bool IsCooldownBased { get; set; }
    
}
