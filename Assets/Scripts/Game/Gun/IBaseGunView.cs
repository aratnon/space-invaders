﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBaseGunView : IView
{
    void SpawnBullet(int gunLevel, BulletModel bulletModel);
    void PlaySpawnBulletSound();
}
