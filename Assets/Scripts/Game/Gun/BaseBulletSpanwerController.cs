﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBulletSpanwerController : BaseController<IBaseBulletSpawnerView>
{
    public void StartSpawnBullet(BulletModel bulletModel)
    {
        _view.SpawnBullet(bulletModel);
    }
}
