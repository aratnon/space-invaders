﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Gun")]
public class GunData : ScriptableObject
{
    [SerializeField] private string bulletId;
    [SerializeField] private int bulletDamage;
    [SerializeField] private float gunDelay;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private bool isCooldownBased;
    
    public string BulletId => bulletId;
    
    public float GunDelay => gunDelay;
    
    public float BulletSpeed => bulletSpeed;
    
    public int BulletDamage => bulletDamage;

    public bool IsCooldownBased => isCooldownBased;
}
