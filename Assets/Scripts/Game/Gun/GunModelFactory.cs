﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunModelFactory
{
    public List<GunModel> CreateGunModels(List<GunData> gunData)
    {
        List<GunModel> gunModels = new List<GunModel>();
        gunData.ForEach(data => gunModels.Add(CreateGunModel(data)));
        return gunModels;
    }
    
    private GunModel CreateGunModel(GunData gunData)
    {
        return GunModel.Create(
            gunData.GunDelay,
            gunData.BulletDamage,
            gunData.BulletSpeed,
            gunData.BulletId,
            gunData.IsCooldownBased);
    }
}
