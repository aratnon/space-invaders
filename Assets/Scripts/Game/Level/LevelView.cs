﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelView : BaseView, ILevelView
{
    private const string ENEMY_KEY = "Enemy";
    private const string GOLD_DROP_POOL_KEY = "Gold Drop";
    
    [Inject] private LevelController _levelController;
    [Inject] private PowerUpDataManager _powerUpDataManager;
    
    [SerializeField] private int goldDropChance;
    [SerializeField] private int minGoldDrop;
    [SerializeField] private int maxGoldDrop;
    [SerializeField] private List<AudioClip> bgms;
    
    [SerializeField] private PlayerView playerViewPrefab;
    [SerializeField] private PowerUpView powerUpPrefab;
    [SerializeField] private BackgroundView backgroundViewPrefab;
    [SerializeField] private List<EnemyWave> enemyWavePrefabs;
    
    private EnemyWave _currentEnemyWave;

    private Action<EventParam> _clickStartEvent;
    private Action<EventParam> _clickRestartEvent;
    private Action<EventParam> _nextWaveEvent;
    private Action<EventParam> _finishWaveEvent;
    private Action<EventParam> _enemyDieEvent;
    private Action<EventParam> _addGoldEvent;
    private Action<EventParam> _gameOverEvent;

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Start()
    {
        _levelController.View = this;
        _levelController.Start(LevelModel.Create(goldDropChance, 
            minGoldDrop, 
            maxGoldDrop,
            bgms.Count));
    }

    private void OnEnable()
    {
        _clickStartEvent = _eventManager.StartListening(EventConstant.CLICK_GAME_START, 
            param => { _levelController.OnClickedStart(CreatePlayerModel()); });
        
        _clickStartEvent = _eventManager.StartListening(EventConstant.CLICK_RESTART, 
            param => { _levelController.OnClickedStart(CreatePlayerModel()); });
        
        _nextWaveEvent = _eventManager.StartListening(EventConstant.NEXT_WAVE, 
            param => { _levelController.StartNextWave(param.GetParam<int>(EnemyWaveModel.WAVE_NUMBER)); });
        
        _finishWaveEvent = _eventManager.StartListening(EventConstant.FINISH_WAVE, 
            param => { _levelController.NextWave(); });
        
        _addGoldEvent = _eventManager.StartListening(EventConstant.ADD_GOLD, 
            param => { _levelController.AddGold(param.GetParam<int>(EventParamConstant.GOLD_AMOUT)); });
        
        _enemyDieEvent = _eventManager.StartListening(EventConstant.ENEMY_DIE,
            param =>
            {
                _levelController.AddScore(param.GetParam<int>(EventParamConstant.GAME_SCORE));
                _levelController.DropGold(param.GetParam<Vector2>(EventParamConstant.ENEMY_POSITION));
            });
        
        _gameOverEvent = _eventManager.StartListening(EventConstant.GAME_OVER, 
            param => { _levelController.GameOver(); });
    }

    private void OnDestroy()
    {
        _eventManager.StopListening(EventConstant.CLICK_GAME_START, _clickStartEvent);
        _eventManager.StopListening(EventConstant.NEXT_WAVE, _nextWaveEvent);
        _eventManager.StopListening(EventConstant.FINISH_WAVE, _finishWaveEvent);
        _eventManager.StopListening(EventConstant.ADD_GOLD, _addGoldEvent);
        _eventManager.StopListening(EventConstant.ENEMY_DIE, _enemyDieEvent);
        _eventManager.StopListening(EventConstant.GAME_OVER, _gameOverEvent);
    }

    public void PlayBackgroundMusic(int bgmIndex)
    {
        PlaySound(bgms[bgmIndex], true);
    }

    private PlayerModel CreatePlayerModel()
    {
        return PlayerModel.Create(_powerUpDataManager.CurrentFuelPowerUpPoint,
            _powerUpDataManager.CurrentPlasmaShieldDuration);
    }

    private void Update()
    {
        _levelController.UpdatePowerUpTime(Time.deltaTime);
        _levelController.UpdateGameScore(Time.deltaTime);
        _levelController.UpdatePlayerShieldTime(Time.deltaTime);
    }

    public void CreatePlayer()
    {
        _unityUtility.CreateGameObject(playerViewPrefab, transform);
    }

    public void CreateBackground()
    {
        _unityUtility.CreateGameObject(backgroundViewPrefab, transform);
    }

    public void CreatePowerUp()
    {
        _unityUtility.CreateGameObject(powerUpPrefab, transform);
    }

    public void CreateEnemy(EnemyModel enemyModel)
    {
        GameObject enemy = _objectPoolManager.GetPoolableObject("Enemy");
        enemy.transform.localPosition = new Vector3(0, 4, 0);
    }

    public void ClearCurrentEnemyWave()
    {
        if (_currentEnemyWave != null)
        {
            Destroy(_currentEnemyWave.gameObject);
        }
    }

    public void ClearObjectsOnScreen()
    {
        _objectPoolManager.DisablePoolableObjectOnGameRestart();
    }

    public void CreateEnemyWave(int waveNumber)
    {
        _currentEnemyWave = _unityUtility.CreateRandomGameObjectFromList(enemyWavePrefabs, transform);
        _currentEnemyWave.StartCreatingEnemy(waveNumber);
    }

    public void DropGold(GoldDropModel goldDropModel)
    {
        var goldDropView = _objectPoolManager.GetComponentFromPoolableObject<GoldDropView>(GOLD_DROP_POOL_KEY);
        goldDropView.DropGold(goldDropModel);
    }
}
