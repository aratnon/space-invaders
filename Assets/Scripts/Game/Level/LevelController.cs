﻿
using UnityEngine;
using Zenject;

public class LevelController : BaseController<ILevelView>
{
    private LevelModel _levelModel;
    
    public void Start(LevelModel levelModel)
    {
        _levelModel = levelModel;
        
        PlaySound(() => _view.PlayBackgroundMusic(GetRandomBgm()));
        _view.CreatePlayer();
        _view.CreateBackground();
        _view.CreatePowerUp();
    }

    private int GetRandomBgm()
    {
        return _gameUtility.RandomRange(0, _levelModel.BgmCount);
    }

    public void StartNextWave(int waveNumber)
    {
        _view.ClearCurrentEnemyWave();
        _view.CreateEnemyWave(waveNumber);
    }

    public void OnClickedStart(PlayerModel playerModel)
    {
        _gameManager.Play(playerModel);
        _view.ClearObjectsOnScreen();
        _view.TriggerEvent(EventConstant.GAME_START);
    }

    public void NextWave()
    {
        _gameManager.NextWave();
    }

    public void AddScore(int score)
    {
        _gameManager.AddScore(score);
    }
    
    public void AddGold(int gold)
    {
        _gameManager.AddGold(gold);
    }

    public void GameOver()
    {
        _saveGameManager.SaveGame.Gold = _gameManager.Gold;
        _saveGameManager.Save();
    }

    public void UpdateGameScore(float dt)
    {
        if (!Playing) return;
        _gameManager.UpdateGameScore(dt);
        
    }

    public void UpdatePowerUpTime(float dt)
    {
        if (!Playing) return;
        _gameManager.UpdatePowerUpTime(dt);
        
        if (!CanCreatePowerUp()) return;
        _gameManager.ResetPowerUpTime();
        TriggerCreatePowerUp();
    }

    private bool CanCreatePowerUp()
    {
        var gameTime = (int) _gameManager.CurrentGameModel.PowerUpTime;
        return gameTime > 0 && gameTime % 7 == 0;
    }

    private void TriggerCreatePowerUp()
    {
        int randomIndex = _gameUtility.RandomRange(0, PowerUpConstant._powerUpTypes.Count);
        PowerUpConstant.PowerUpType type = PowerUpConstant._powerUpTypes[randomIndex];
        
        EventParam eventParam = EventParam.Create();
        eventParam.AddParam(EventParamConstant.POWER_UP_TYPE, type);
        
        _view.TriggerEvent(EventConstant.ACTIVATE_POWER_UP, eventParam);
    }

    public void UpdatePlayerShieldTime(float dt)
    {
        if (Playing)
        {
            if (!_gameManager.PlayerModel.PlasmaShieldActive) return;
            _gameManager.UpdatePlayerShieldTime(dt);
        }
    }

    public void DropGold(Vector2 enemyPosition)
    {
        if (CanDropGold())
        {
            var goldDropModel = GoldDropModel.Create(GetGoldDropAmount(), enemyPosition);
            _view.DropGold(goldDropModel);
        }
    }

    public bool CanDropGold()
    {
        return _gameUtility.RandomRange(0, 100) <= _levelModel.GoldDropChance;
    }

    public int GetGoldDropAmount()
    {
        return _gameUtility.RandomRange(_levelModel.MinGoldDrop, _levelModel.MaxGoldDrop);
    }
    
}
