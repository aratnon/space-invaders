﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILevelView: IView
{
    void PlayBackgroundMusic(int bgmIndex);
    
    void CreatePlayer();
    void CreateBackground();

    void CreatePowerUp();

    void CreateEnemy(EnemyModel enemyModel);

    void ClearCurrentEnemyWave();
    
    void ClearObjectsOnScreen();
    void CreateEnemyWave(int waveNumber);

    void DropGold(GoldDropModel goldDropModel);

}
