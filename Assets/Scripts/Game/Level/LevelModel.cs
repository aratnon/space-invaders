﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelModel
{
    public static LevelModel Create(int goldDropChance,
        int minGoldDrop,
        int maxGoldDrop,
        int bgmCount)
    {
        return new LevelModel
        {
            GoldDropChance = goldDropChance,
            MinGoldDrop = minGoldDrop,
            MaxGoldDrop = maxGoldDrop,
            BgmCount = bgmCount
        };
    }

    public int GoldDropChance { get; private set; }
    public int MinGoldDrop { get; private set; }
    public int MaxGoldDrop { get; private set; }
    
    public int BgmCount { get; set; }
}
