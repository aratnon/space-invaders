﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GoldDropView : BaseView, IGoldDropView
{
    [Inject] private GoldDropController _goldDropController;

    [SerializeField] private float moveSpeed;
    [SerializeField] private AudioClip pickSound;
    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        _goldDropController.View = this;
    }

    public void DropGold(GoldDropModel model)
    {
        model.MoveSpeed = moveSpeed;
        _goldDropController.Start(model);
    }

    private void Update()
    {
        _goldDropController.UpdatePosition();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        _goldDropController.OnTriggerEntered(collision.tag);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        _goldDropController.OnTriggerExit(collision.tag);
    }

    public void PlayPickSound()
    {
        PlaySound(pickSound, false);
    }

    public void SetInitialPosition(Vector2 position)
    {
        transform.position = position;
    }
    
    public void StartMoving(float moveSpeed)
    {
        transform.Translate(Time.deltaTime * moveSpeed * Vector3.up); 
    }
}
