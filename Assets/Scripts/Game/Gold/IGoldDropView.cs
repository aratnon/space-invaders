﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGoldDropView : IView
{
    void PlayPickSound();
    void SetInitialPosition(Vector2 position);
    void StartMoving(float moveSpeed);
}
