﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldDropModel
{
    public static GoldDropModel Create(int gold, Vector2 dropPosition)
    {
        return new GoldDropModel
        {
            Gold = gold,
            DropPosition = dropPosition,
            Activate = true
        };
    }
    
    public int Gold { get; set; }
    
    public float MoveSpeed { get; set; }
    public Vector2 DropPosition { get; set; }
    
    public bool Activate { get; set; }
}
