﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoldDropController : BaseController<IGoldDropView>
{
    private GoldDropModel _goldDropModel;

    public void Start(GoldDropModel goldDropModel)
    {
        _goldDropModel = goldDropModel;
        _view.SetInitialPosition(goldDropModel.DropPosition);
    }
    
    public void UpdatePosition()
    {
        if (_goldDropModel.Activate)
        {
            _view.StartMoving(_goldDropModel.MoveSpeed);
        }
    }
    
    public void OnTriggerEntered(string tag)
    {
        if (tag == Tag.PLAYER)
        {
            PlaySound(() => _view.PlayPickSound());
            
            EventParam eventParam = EventParam.Create();
            eventParam.AddParam(EventParamConstant.GOLD_AMOUT, _goldDropModel.Gold);
            
            _view.TriggerEvent(EventConstant.ADD_GOLD, eventParam);
            
            _goldDropModel.Activate = false;
            _view.Hide();
        }
    }

    public void OnTriggerExit(string tag)
    {
        if (tag == Tag.BOUNDARY)
        {
            _goldDropModel.Activate = false;
            _view.Hide();
        }
    }
}
