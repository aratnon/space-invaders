﻿
using NSubstitute;
using NUnit.Framework;
using Zenject;

namespace Tests
{
    public class PlayerControllerTest: ZenjectUnitTestFixture
    {
        private MainMenuController _mainMenuController;
        private PlayerController _controller;
        private IPlayerView _view;
        private PlayerModel _playerModel;
        
        [SetUp]
        public void Start()
        {
            TestInstaller.Install(Container);

            _mainMenuController = Container.Resolve<MainMenuController>();
            _controller = Container.Resolve<PlayerController>();
            _view = Substitute.For<IPlayerView>();
            _controller.View = _view;
            _controller.UnitModel = PlayerModel.Create(1, 1);
        }

        [Test]
        public void ShieldShouldBeDeactivatedWhenStarting()
        {
            _controller.Start();
            
            _view.Received().SetActiveShield(false);
        }

        [Test]
        public void ShouldTriggerUpdateFuelBarEventWhenHittingByABullet()
        {
            var param = EventParam.Create();
            param.AddParam(EventParamConstant.BULLET_DAMAGE, 1);
            
            _controller.OnHitByBullet(param);

            _view.Received().TriggerEvent(EventConstant.UPDATE_PLAYER_FUEL_LEVEL);
        }

        [Test]
        public void ShouldTriggerUpdateFuelBarEventWhenPickingUpAHealthPowerUp()
        {
            _controller.OnHitByPowerUp(PowerUpConstant.PowerUpType.Health);
            
            _view.Received().TriggerEvent(EventConstant.UPDATE_PLAYER_FUEL_LEVEL);
        }

        [Test]
        public void ShouldActiveShieldWhenPickingUpAShieldPowerUp()
        {
            _controller.OnHitByPowerUp(PowerUpConstant.PowerUpType.Shield);
            
            _view.Received().SetActiveShield(true);
        }

        [Test]
        public void ShouldNotUpdateFuelWhenTheGameIsNotPlaying()
        {
            _view.DidNotReceive();
        }

        [Test]
        public void ShieldShouldBeDeactivated()
        {
            _controller.OnShieldDeactivated();
            
            _view.Received().SetActiveShield(false);
        }

        [Test]
        public void ShouldHideAfterDying()
        {
            _controller.OnDied();
            
            _view.Received().Hide();
        }
    }
}
