﻿using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;

namespace Tests
{
    public class GoldDropControllerTest : ZenjectUnitTestFixture
    {
        private GoldDropController _controller;
        private IGoldDropView _view;
        
        [SetUp]
        public void Start()
        {
            TestInstaller.Install(Container);
        
            _controller = Container.Resolve<GoldDropController>();
            _view = Substitute.For<IGoldDropView>();
            _controller.View = _view;
        }

        [Test]
        public void ShouldSetInitialPositionWhenStart()
        {
            var goldDropModel = GoldDropModel.Create(1, new Vector2(1, 1));
            
            _controller.Start(goldDropModel);
            
            _view.Received().SetInitialPosition(goldDropModel.DropPosition);
        }

        [Test]
        public void ShouldMoveWhenActivated()
        {
            var goldDropModel = GoldDropModel.Create(1, new Vector2(1, 1));
            
            _controller.Start(goldDropModel);
            _controller.UpdatePosition();
            
            _view.Received().StartMoving(goldDropModel.MoveSpeed);
        }

        [Test]
        public void ShouldPlayPickSoundWhenHitPlayer()
        {
            var goldDropModel = GoldDropModel.Create(1, new Vector2(1, 1));
            
            _controller.Start(goldDropModel);
            _controller.OnTriggerEntered(Tag.PLAYER);
            
            _view.Received().PlayPickSound();
        }

        [Test]
        public void ShouldTriggerAddGoldEventWhenHitPlayer()
        {
            var goldDropModel = GoldDropModel.Create(1, new Vector2(1, 1));
            
            _controller.Start(goldDropModel);
            _controller.OnTriggerEntered(Tag.PLAYER);
            
            _view.Received().TriggerEvent(EventConstant.ADD_GOLD, Arg.Any<EventParam>());
        }

        [Test]
        public void ShouldHideWhenHitPlayer()
        {
            var goldDropModel = GoldDropModel.Create(1, new Vector2(1, 1));
            
            _controller.Start(goldDropModel);
            _controller.OnTriggerEntered(Tag.PLAYER);
            
            _view.Received().Hide();
        }

        [Test]
        public void ShouldHideWhenHitBoundary()
        {
            var goldDropModel = GoldDropModel.Create(1, new Vector2(1, 1));
            
            _controller.Start(goldDropModel);
            _controller.OnTriggerExit(Tag.BOUNDARY);
            
            _view.Received().Hide();
        }
    }
}
