﻿using NSubstitute;
using NUnit.Framework;
using UnityEngine.TestTools;

namespace Tests
{
    public class BulletTest
    {
        private BulletController _controller;
        private IBulletView _view;
        
        [SetUp]
        public void Setup()
        {
            _view = Substitute.For<IBulletView>();

            _controller = new BulletController { View = _view };
        }

        [Test]
        public void OnSpawnedTest()
        {
            _controller.OnSpawned(new BulletModel());

            Assert.IsTrue(_controller.HasSpawned);
            Assert.NotNull(_controller.BulletModel);
        }

        [Test]
        public void BulletShouldMoveAfterSpawning()
        {
            BulletModel bulletModel = new BulletModel { Speed = 5.0f };

            _controller.OnSpawned(bulletModel);
            _controller.UpdateBullet();
            
            _view.Received().Move(bulletModel.Speed);
        }

        [Test]
        public void ShouldHideWhenLeavingTheBoundary()
        {
            _controller.OnTriggerExited(Tag.BOUNDARY);
            
            _view.Received().Hide();
        }

        [Test]
        public void ShouldReturnEnemyBulletHitTagWhenTheTagIsEnemy()
        {
            var name = "name";
            var hitTag = _controller.GetBulletHitEventTag(Tag.ENEMY, name);

            Assert.AreEqual(hitTag, EventConstant.GetEnemyBulletHitEventTag(name));
        }
        
        [Test]
        public void ShouldReturnPlayerBulletHitTagWhenTheTagIsPlayer()
        {
            var name = "name";
            var hitTag = _controller.GetBulletHitEventTag(Tag.PLAYER, name);

            Assert.AreEqual(hitTag, EventConstant.GetPlayerBulletHitEventTag());
        }

        [Test]
        public void CreateHitParamTest()
        {
            _controller.BulletModel = new BulletModel { Damage = 1 };
            
            var param = _controller.CreateHitParam();
            var damage = param.GetParam<int>(EventParamConstant.BULLET_DAMAGE);
            
            Assert.AreEqual(_controller.BulletModel.Damage, damage);
        }

        [Test]
        public void EnemyBulletShouldHideAfterHittingAShield()
        {
            _controller.BulletModel = new BulletModel { Source = Tag.ENEMY };
            
            _controller.OnTriggerEntered(Tag.SHIELD, "");
            
            _view.Received().Hide();
        }
        
        [Test]
        public void EnemyBulletShouldDealDamageToPlayerAfterHitting()
        {
            _controller.BulletModel =  new BulletModel { Source = Tag.ENEMY, Damage = 1};
            
            _controller.OnTriggerEntered(Tag.PLAYER, "");
            
            _view.Received().TriggerEvent(Arg.Any<string>(), Arg.Any<EventParam>());
            _view.Received().Hide();
        }
        
        [Test]
        public void PlayerBulletShouldDealDamageToEnemyAfterHitting()
        {
            _controller.BulletModel =  new BulletModel { Source = Tag.PLAYER, Damage = 1};
            
            _controller.OnTriggerEntered(Tag.ENEMY, "");
            
            _view.Received().TriggerEvent(Arg.Any<string>(), Arg.Any<EventParam>());
            _view.Received().Hide();
        }
    }
}