﻿using NSubstitute;
using NUnit.Framework;
using Zenject;

namespace Tests
{
    public class PowerUpTest : ZenjectUnitTestFixture
    {
        private PowerUpController _controller;
        private IPowerUpView _view;

        [SetUp]
        public void Start()
        {
            TestInstaller.Install(Container);
        
            _controller = Container.Resolve<PowerUpController>();
            _view = Substitute.For<IPowerUpView>();
            _controller.View = _view;
        }

        [Test]
        public void ShouldSetInitialPositionWhenStarting()
        {
            _controller.Start();
            
            _view.Received().SetInitialPosition(Arg.Any<float>(),Arg.Any<float>());
        }

        [Test]
        public void ShouldPlaySoundWhenHitPlayer()
        {
            _controller.OnTriggerEntered(Tag.PLAYER);
            
            _view.Received().PlayPickSound();
        }

        [Test]
        public void ShouldTriggerPowerUpHitPlayerEventWhenHitPlayer()
        {
            _controller.OnTriggerEntered(Tag.PLAYER);
            
            _view.Received().TriggerEvent(EventConstant.POWER_UP_HIT_PLAYER, Arg.Any<EventParam>());
        }

        [Test]
        public void ShouldResetPositionWhenHitPlayer()
        {
            _controller.OnTriggerEntered(Tag.PLAYER);
            
            _view.Received().SetInitialPosition(Arg.Any<float>(),Arg.Any<float>());
        }

        [Test]
        public void ShouldSetGunUpgradeSprite()
        {
            _controller.OnPowerUpActivated(PowerUpConstant.PowerUpType.GunUpgrade);
            
            _view.Received().SetGunUpgradeSprite();
        }
        
        [Test]
        public void ShouldSetHealthSprite()
        {
            _controller.OnPowerUpActivated(PowerUpConstant.PowerUpType.Health);
            
            _view.Received().SetHealthSprite();
        }
        
        [Test]
        public void ShouldSetShieldSprite()
        {
            _controller.OnPowerUpActivated(PowerUpConstant.PowerUpType.Shield);
            
            _view.Received().SetShieldSprite();
        }

        [Test]
        public void ShouldResetPositionWhenExitBoundary()
        {
            _controller.OnTriggerExited(Tag.BOUNDARY);
            
            _view.Received().SetInitialPosition(Arg.Any<float>(),Arg.Any<float>());
        }
    }
}
