﻿using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Zenject;

namespace Tests
{
    public class ShopControllerTest : ZenjectUnitTestFixture
    {
        private ShopController _controller;
        private IShopView _view;
        
        [SetUp]
        public void Start()
        {
            TestInstaller.Install(Container);
        
            _controller = Container.Resolve<ShopController>();
            _view = Substitute.For<IShopView>();
            _controller.View = _view;
        }

        private List<BasePowerUpModel> CreateModels()
        {
            var model = new FuelPowerUpUpgradeModel
            {
                CurrentLevel = 0,
                MaxLevel = 0,
                Key = "FUEL_UPGRADE",
                UpgradeCosts = new List<int>{ 1, 2, 3}
            };
            
            var models = new List<BasePowerUpModel> {
                model, 
                model, 
                model
            };

            return models;
        }

        [Test]
        public void ShouldCreateShopItemExactTheSameAmountAsModelCount()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            
            _view.Received(models.Count).CreateShopItem(Arg.Any<int>());
        }

        [Test]
        public void ShouldUpdateListHeightWhenStart()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            
            _view.Received().UpdateListHeight();
        }
        
        [Test]
        public void ShouldDisableMainPanelWhenStart()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            
            _view.Received().SetActiveMainPanel(false);
        }

        [Test]
        public void ShouldActivateMainPanelWhenShow()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.Show();
            
            _view.Received().SetActiveMainPanel(true);
        }

        [Test]
        public void ShouldUpdateGoldWhenShow()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.Show();
            
            _view.UpdateGold(Arg.Any<string>());
        }

        [Test]
        public void ShouldUpdateShopItemWhenShow()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.Show();
            
            _view.Received(models.Count).UpdateShopItem(Arg.Any<int>(), 
                Arg.Any<bool>(),
                Arg.Any<BasePowerUpModel>());
        }
        
        [Test]
        public void ShouldUpdateListHeightWhenShow()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            
            _view.Received().UpdateListHeight();
        }

        [Test]
        public void GoldShouldBeEnoughWhenUpgradePriceIsLower()
        {
            var saveGame = new SaveGame { Gold = 100 };
            var model = new FuelPowerUpUpgradeModel
            {
                CurrentLevel = 0,
                MaxLevel = 4,
                UpgradeCosts = new List<int> {1, 2, 3, 4}
            };
            var goldEnough = _controller.IsGoldEnough(model, saveGame);
            
            Assert.AreEqual(goldEnough, true);
        }
        
        [Test]
        public void GoldShouldNotBeEnoughWhenUpgradePriceIsHiehger()
        {
            var saveGame = new SaveGame { Gold = 0 };
            var model = new FuelPowerUpUpgradeModel
            {
                CurrentLevel = 0,
                MaxLevel = 4,
                UpgradeCosts = new List<int> {1, 2, 3, 4}
            };
            var goldEnough = _controller.IsGoldEnough(model, saveGame);
            
            Assert.AreEqual(goldEnough, false);
        }
        
        [Test]
        public void GoldShouldBeEnoughWhenUpgradeIsAtMaxLevel()
        {
            var saveGame = new SaveGame { Gold = 0 };
            var model = new FuelPowerUpUpgradeModel
            {
                CurrentLevel = 4,
                MaxLevel = 4,
                UpgradeCosts = new List<int> {1, 2, 3, 4}
            };
            var goldEnough = _controller.IsGoldEnough(model, saveGame);
            
            Assert.AreEqual(goldEnough, true);
        }

        [Test]
        public void ShouldUpdateGoldWhenUpgradePowerUp()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.UpgradePowerUp(1);
            
            _view.Received().UpdateGold(Arg.Any<string>());
        }

        [Test]
        public void ShouldPlaySpendingSoundWhenUpgradePowerUp()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.UpgradePowerUp(1);
            
            _view.Received().PlaySpendGoldSound();
        }

        [Test]
        public void ShouldUpdateShopItemWhenUpgradePowerUp()
        {
            var upgradeIndex = 1;
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.UpgradePowerUp(upgradeIndex);
            
            _view.Received().UpdateShopItem(upgradeIndex, 
                Arg.Any<bool>(), 
                Arg.Any<BasePowerUpModel>());
        }

        [Test]
        public void ShouldUpgradeListHeightWhenUpgradePowerUp()
        {
            var models = CreateModels();
            
            _controller.Start(models);
            _controller.UpgradePowerUp(1);
            
            _view.Received().UpdateListHeight();
        }
    }
}
