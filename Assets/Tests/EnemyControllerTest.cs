﻿using System.Collections;
using System.Collections.Generic;
using NSubstitute;
using NUnit.Framework;
using UnityEngine;
using Zenject;

namespace Tests
{
    public class EnemyControllerTest : ZenjectUnitTestFixture
{
    private EnemyController _controller;
    private IEnemyView _view;

    [SetUp]
    public void Start()
    {
        TestInstaller.Install(Container);
        
        _controller = Container.Resolve<EnemyController>();
        _view = Substitute.For<IEnemyView>();
        _controller.View = _view;
    }

    [Test]
    public void ShouldSetEnemyToInitialPositionWhenStarting()
    {
        _controller.Start(Arg.Any<EnemyModel>());
        
        _view.Received().SetInitialPosition();
    }

    [Test]
    public void ShouldNotUpdateMovingWhenEnemyIsNotMoving()
    {
        var enemyModel = new EnemyModel
        {
            MaxHealth = 10,
            CurrentHealth = 10,
            Moving = false
        };

        _controller.Start(enemyModel);
        _controller.UpdateMoving();
        _view.DidNotReceive();
    }
    
    [Test]
    public void ShouldNotUpdateMovingWhenEnemyIsDead()
    {
        var enemyModel = new EnemyModel
        {
            Moving = true,
            MaxHealth = 10,
            CurrentHealth = 0
        };
        
        _controller.Start(enemyModel);
        _controller.UpdateMoving();
        _view.DidNotReceive();
    }

    [Test]
    public void ShouldUpdateMovingWhenEnemyIsMovingAndNotDead()
    {
        var enemyModel = new EnemyModel
        {
            Moving = true,
            MaxHealth = 10,
            CurrentHealth = 10
        };
        
        _controller.Start(enemyModel);
        _controller.UpdateMoving();
        _view.Received().MoveAlongPath(enemyModel.CurrentPathIndex, enemyModel.MoveSpeed);
    }

    [Test]
    public void ShouldNotDoAnythingWhenReachingToTheLastPath()
    {
        var enemyModel = new EnemyModel
        {
            CurrentPathIndex = 1,
            TotalPath = 1
        };
        
        _controller.Start(enemyModel);
        _controller.OnReachedNextPath();

        _view.DidNotReceive();
    }

    [Test]
    public void ShouldFireWHenReachingToFiringPath()
    {
        var enemyModel = new EnemyModel
        {
            FiringLocations = new List<int>{ 0 },
            CurrentPathIndex = 0,
            TotalPath = 4
        };
        _controller.Start(enemyModel);
        _controller.OnReachedNextPath();
        
        _view.Received().Fire();
    }

    [Test]
    public void ShouldIncreaseCurrentPathWhenNotReachingToTheLastPath()
    {
        var currentPath = 1;
        var enemyModel = new EnemyModel
        {
            FiringLocations = new List<int>(),
            CurrentPathIndex = currentPath,
            TotalPath = 4
        };
        
        _controller.Start(enemyModel);
        _controller.OnReachedNextPath();
        
        Assert.AreEqual(currentPath + 1, _controller.UnitModel.CurrentPathIndex);
    }

    [Test]
    public void ShouldContinueMovingWhenCurrentPathIndexIsLessThanTotalPath()
    {
        var currentPath = 1;
        var enemyModel = new EnemyModel
        {
            FiringLocations = new List<int>(),
            CurrentPathIndex = currentPath,
            TotalPath = 4
        };
        
        _controller.Start(enemyModel);
        _controller.OnReachedNextPath();
        
        Assert.AreEqual(_controller.UnitModel.Moving, true);
    }
    
    [Test]
    public void ShouldStopMovingWhenCurrentPathIndexIsGreaterThanTotalPath()
    {
        var currentPath = 3;
        var enemyModel = new EnemyModel
        {
            FiringLocations = new List<int>(),
            CurrentPathIndex = currentPath,
            TotalPath = 4
        };
        
        _controller.Start(enemyModel);
        _controller.OnReachedNextPath();
        
        Assert.AreEqual(_controller.UnitModel.Moving, false);
    }

    [Test]
    public void ShouldHideWhenStopMoving()
    {
        var currentPath = 3;
        var enemyModel = new EnemyModel
        {
            FiringLocations = new List<int>(),
            CurrentPathIndex = currentPath,
            TotalPath = 4
        };
        
        _controller.Start(enemyModel);
        _controller.OnReachedNextPath();
        
        _view.Received().TriggerEvent(EventConstant.FINISHED_ENEMY);
        _view.Received().StopListeningEvent();
        _view.Received().Hide();
    }

    [Test]
    public void ShouldTriggerFinishWhenDying()
    {
        _controller.Start(new EnemyModel());
        
        _controller.OnDied();
        
        _view.TriggerEvent(EventConstant.FINISHED_ENEMY);
    }
    
    [Test]
    public void ShouldTriggerAddScoreWhenDying()
    {
        _controller.Start(new EnemyModel());
        _controller.OnDied();
        
        _view.Received().TriggerEvent(EventConstant.ENEMY_DIE, Arg.Any<EventParam>());
    }

    [Test]
    public void ShouldHideWhenDying()
    {
        _controller.Start(new EnemyModel());
        
        _controller.OnDied();
        
        _view.Received().Hide();
    }
}
}




